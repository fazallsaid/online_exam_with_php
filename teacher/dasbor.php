<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Sukses!</h4>
                <?php echo $notif; ?>
              </div>

<div class="row">
    <div class="col-md-12">
    <div class="box box-default">
          <div class="box-header with-border">
            <h3 class="box-title">Informasi Pengguna</h3>
          </div>
          <div class="box-body">
            Nama: <b><?php echo $_SESSION['nama_guru']; ?>, <?php echo $_SESSION['gelar']; ?><br/></b>
            NIP: <b><?php echo $_SESSION['nip']; ?><br/></b>
          </div>
          <!-- /.box-body -->
        </div>
    </div>
</div>