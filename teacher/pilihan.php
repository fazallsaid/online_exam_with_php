<?php
if (empty($_GET['page'])) {
    include "dasbor.php";
} else {
    switch ($_GET['page']) {

            //pelanggan
        case ('data_soal'):
            include('pages/soal/data_soal.php');
            break;
        case ('tambah_soal'):
            include('pages/soal/tambah_soal.php');
            break;
        case ('edit_soal'):
            include('pages/soal/edit_soal.php');
            break;
        case ('proses_edit_soal'):
            include('pages/soal/proses_update.php');
            break;
        case ('hapus_soal'):
            include('pages/soal/hapus_soal.php');
            break;
        case ('proses_soal'):
            include('pages/soal/images/upload_soal.php');
            break;
        case ('my_profile'):
            include('pages/profil/profilku.php');
            break;
        case ('update_profil'):
            include('pages/profil/proses_edit.php');
            break;
        case ('keluar'):
            include('logout.php');
            break;
        case ('update_soal_aktif'):
            include('pages/soal/proses_edit.php');
            break;
        case ('data_nilai_siswa'):
            include('pages/nilai/lihat_nilai.php');
            break;
        case ('lihat_nilai_per_kelas'):
            include('pages/nilai/lihat_nilai_lanjut.php');
            break;
        case ('lihat_nilai_per_mapel'):
            include('pages/nilai/lihat_nilai_per_mapel.php');
            break;

        default:
            include('dasbor.php');
    }
}
