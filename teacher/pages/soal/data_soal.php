<?php
include "../adminis/config.php";
$query = mysqli_query($connection, "SELECT soal.*, mapel.*, guru.*, kelas.*
FROM soal
JOIN guru ON soal.nip=guru.nip
JOIN kelas ON soal.id_kelas=kelas.id_kelas
JOIN mapel ON soal.id_mapel=mapel.id_mapel
WHERE mapel.nip='$_SESSION[nip]'");
?>



<div class="box">
  <div class="box-header">
    <h3 class="Data Guru">Data Soal</h3> <a type="button" class="btn btn-success" href="./?page=tambah_soal">Tambah Data Soal</a>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <table id="example1" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>No</th>
          <th>Thumb</th>
          <th>Mata Pelajaran</th>
          <th>Uraian Soal</th>
          <th>Nama Guru</th>
          <th>Kelas</th>
          <th>Aktif</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tbody>
        <?php if (mysqli_num_rows($query) > 0) { ?>
          <?php
            $no = 1;
            while ($data = mysqli_fetch_array($query)) {
              ?>

            <?php
                if ($data['gambar'] == null) {
                  $pic = "";
                } else {
                  $pic = "<img src='foto_soal/$data[gambar]' width='30' height='20' />";
                }
                ?>
            <tr>
              <!-- <td></td> -->
              <td><?php echo $no ?></td>
              <td><?php echo $pic; ?></td>
              <td>
                <font face="trebuchet MS"><?php echo $data["nama_mapel"]; ?></font>
              </td>
              <td>
                <font face="trebuchet MS"><?php echo $data["nama_soal"]; ?></font>
              </td>
              <td>
                <font face="trebuchet MS"><?php echo $data["nama_guru"]; ?>, <?php echo $data["gelar"]; ?></font>
              </td>
              <td>
                <font face="trebuchet MS"><?php echo $data["nama_kelas"]; ?> <?php echo $data["abjad_kelas"]; ?></font>
              </td>
              <?php
                  if ($data["aktif"] == "Y") {
                    $soal_aktif = "Nonaktifkan";
                    $aktif = "N";
                    $s_aktif = "Ya";
                    $class = "class='btn btn-danger'";
                  } else {
                    $soal_aktif = "Aktifkan";
                    $aktif = "Y";
                    $s_aktif = "Tidak";
                    $class = "class='btn btn-success'";
                  }
                  ?>
              <td>
                <font face="trebuchet MS"><?php echo $s_aktif; ?></font>
                <br /><br />
                <form action="?page=update_soal_aktif" method="post">
                  <input type="hidden" name="id_soal" value="<?php echo $data["id_soal"]; ?>">
                  <input type="hidden" name="nama_soal" value="<?php echo $data["nama_soal"]; ?>">
                  <input type="hidden" name="a" value="<?php echo $data["a"]; ?>">
                  <input type="hidden" name="b" value="<?php echo $data["b"]; ?>">
                  <input type="hidden" name="c" value="<?php echo $data["c"]; ?>">
                  <input type="hidden" name="d" value="<?php echo $data["d"]; ?>">
                  <input type="hidden" name="id_mapel" value="<?php echo $data["id_mapel"]; ?>">
                  <input type="hidden" name="id_kelas" value="<?php echo $data["id_kelas"]; ?>">
                  <input type="hidden" name="nip" value="<?php echo $data["nip"]; ?>">
                  <input type="hidden" name="kunci_jawaban" value="<?php echo $data["kunci_jawaban"]; ?>">
                  <input type="hidden" name="gambar" value="<?php echo $data["gambar"]; ?>">
                  <input type="hidden" name="aktif" value="<?php echo $aktif; ?>">
                  <button type="submit" <?php echo $class; ?>><?php echo $soal_aktif; ?></button>
                </form>
              </td>
              <td>
                <a href="?page=edit_soal&id_soal=<?php echo $data['id_soal']; ?>"><i class="fa fa-edit edu-checked-pro" aria-hidden="true" style="color: green; font-size: 15px"></i></a> |
                <a href="pages/soal/hapus_soal.php?id_soal=<?php echo $data['id_soal']; ?>" onClick="return confirm('Apakah anda yakin untuk menghapus soal ini? (<?php echo $data['id_soal']; ?>)');"><i class="fa fa-trash edu-checked-pro" aria-hidden="true" style="color: red; font-size: 15px"></i></a>
              </td>
            </tr>
        <?php
            $no++;
          }
        }
        ?>
      </tbody>
    </table>
  </div>
  <!-- /.box-body -->
</div>