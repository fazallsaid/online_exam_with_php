<div class="row">
    <div class="col-md-8">
<div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Isi Data Soal</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" action="?page=proses_soal" method="post" enctype="multipart/form-data">
                <!-- text input -->
                <div class="form-group">
                  <label>Uraian Soal</label>
                  <textarea class="form-control" id="editor1" rows="15" name="nama_soal" placeholder="Masukkan Soal ..."></textarea>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Jawaban Huruf A</label>
                      <textarea class="textarea" id="editor1" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" rows="5" name="a" placeholder="Masukkan Jawaban Huruf A ..."></textarea>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Jawaban Huruf B</label>
                      <textarea class="textarea" id="editor1" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" rows="5" name="b" placeholder="Masukkan Jawaban Huruf B ..."></textarea>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Jawaban Huruf C</label>
                      <textarea class="textarea" id="editor1" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" rows="5" name="c" placeholder="Masukkan Jawaban Huruf C ..."></textarea>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Jawaban Huruf D</label>
                      <textarea class="textarea" id="editor1" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" rows="5" name="d" placeholder="Masukkan Jawaban Huruf D ..."></textarea>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label>Nama Mapel</label>
                  <select class="form-control" name="id_mapel">
                  <option value="show-all" selected="selected">= Pilih Mapel =</option>
                                            <?php
                                                            require_once '../adminis/pengaturan.php';
                                                            $stmt = $db->prepare("SELECT * FROM mapel WHERE nip='$_SESSION[nip]'");
                                                            $stmt->execute();
                                                            ?>
                                            <?php while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) : ?>
                                            <?php extract($row); ?>
                                            <option value="<?php echo $row['id_mapel']; ?>">
                                                <?php echo $row['nama_mapel']; ?>
                                            </option>
                                            <?php endwhile; ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Nama Kelas</label>
                  <select class="form-control" name="id_kelas">
                  <option value="show-all" selected="selected">= Pilih Kelas =</option>
                                            <?php
                                                            require_once '../adminis/pengaturan.php';
                                                            $stmt = $db->prepare("SELECT mapel.id_kelas, kelas.nama_kelas, kelas.abjad_kelas
                                                            FROM mapel
                                                            JOIN kelas ON mapel.id_kelas=kelas.id_kelas 
                                                            WHERE mapel.nip='$_SESSION[nip]'");
                                                            $stmt->execute();
                                                            ?>
                                            <?php while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) : ?>
                                            <?php extract($row); ?>
                                            <option value="<?php echo $row['id_kelas']; ?>">
                                                <?php echo $row['nama_kelas']; ?> <?php echo $row['abjad_kelas']; ?>
                                            </option>
                                            <?php endwhile; ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>NIP Guru</label>
                  <input type="text" class="form-control" name="nip" value="<?php echo $_SESSION['nip']; ?>" readonly>
                </div>
                <div class="form-group">
                      <label>Kunci Jawaban</label>
                      <input type="text" class="form-control" name="kunci_jawaban" placeholder="Masukkan Kunci Jawaban ...">
                    </div>
                <div class="form-group">
                  <label for="exampleInputFile">Gambar Soal</label>
                    <input type="file" id="exampleInputFile" name="gambar">

                  <p class="help-block">Masukkan gambar jika ada.</p>
                </div>
                <div class="form-group">
                  <label>Aktif</label>
                  <select class="form-control" name="aktif">
                  <option value="Y">YA</option>
                  <option value="N">TIDAK</option>
                  </select>
                </div>
                <div class="form-group">
                <button type="submit" class="btn btn-success">Simpan</button>
                </div>


              </form>
            </div>
            <!-- /.box-body -->
          </div>
</div>
<div class="col-md-4">
<div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title"><b>Tips!</b></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><p>Saya Mengerti/Saya kurang paham!</p></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <b>Random Password</b>: Digunakan untuk me-generate password tanpa harus mengetik password untuk guru.<br/>
              Isikan Data Guru sesuai pendaftarannya. NIP guru akan otomatis mengikuti urutan kapan data
              guru di input.
            </div>
            <!-- /.box-body -->
          </div>
</div>
</div>