<br/><br/>
<br/><br/>
<br/><br/>
<br/><br/>
<br/><br/>
<br/>
<center>
<?php
$nama_soal = $_POST['nama_soal'];
$a = $_POST['a'];
$b = $_POST['b'];
$c = $_POST['c'];
$d = $_POST['d'];
$id_mapel = $_POST['id_mapel'];
$id_kelas = $_POST['id_kelas'];
$nip = $_POST['nip'];
$kunci_jawaban = $_POST['kunci_jawaban'];
$aktif = $_POST['aktif'];

$gambar = $_FILES['gambar']['name'];
$tmpName = $_FILES['gambar']['tmp_name'];
$size = $_FILES['gambar']['size'];
$type = $_FILES['gambar']['type'];

$maxsize = 9900000000000;
$typeYgBoleh = array("image/jpeg", "image/png", "image/pjpeg");

$dirFoto = "foto_soal";
if (!is_dir($dirFoto))
	mkdir($dirFoto);
$fileTujuanFoto = $dirFoto . "/" . $gambar;

$dirThumb = "thumb_foto_soal";
if (!is_dir($dirThumb))
	mkdir($dirThumb);
$fileTujuanThumb = $dirThumb . "/t_" . $gambar;

$dataValid = "YA";

if ($size > 0) {
	if ($size > $maxsize) {
		echo "Ukuran File Terlalu besar! <br/>";
		$dataValid = "TIDAK";
	}
	if (!in_array($type, $typeYgBoleh)) {
		echo "Maaf, Kami tidak mengenal file tersebut! <br/>";
		$dataValid = "TIDAK";
	}
}

if (strlen(trim($nama_soal)) == 0) {
	echo "Masih Ada Kesalahan. Silahkan diulangi kembali. <br />";
	$dataValid = "TIDAK";
}
if (strlen(trim($a)) == 0) {
	echo "Masih Ada Kesalahan. Silahkan Diulangi kembali. <br />";
	$dataValid = "TIDAK";
}
if ($dataValid == "TIDAK") {
	echo "Masih Ada Kesalahan, silakan perbaiki! </br>";
	echo "<input type='button' value='Kembali'
		onClick='self.history.back()'>";
	exit;
}
include "koneksi2.php";

$cek = mysqli_num_rows(mysqli_query($kon,"SELECT * FROM soal WHERE nama_soal='$nama_soal'"));
if ($cek > 0){
    echo "<script>window.alert('uraian soal yang anda masukan sudah ada')
    window.location='index.php?page=tambah_soal'</script>";
    }else{
		$sql = "insert into soal (nama_soal, a, b, c, d, id_mapel, id_kelas, nip, kunci_jawaban, gambar, aktif) values ('$nama_soal', '$a', '$b', '$c', '$d', '$id_mapel', '$id_kelas', '$nip', '$kunci_jawaban', '$gambar', '$aktif') ";
		$hasil = mysqli_query($kon, $sql);
	} 

if ($size > 0) {
	if (!move_uploaded_file($tmpName, $fileTujuanFoto)) {
		echo "Gagal Upload Gambar! <br/>";
		echo "<a href='?page=tambah_soal'>Kembali ke halaman input soal.</a>";
		exit;
	} else {
		buat_thumbnail($fileTujuanFoto, $fileTujuanThumb);
	}
}
echo "<br/>File Sudah Diupload! <br/>";

function buat_thumbnail($file_src, $file_dst)
{
	list($w_src, $h_src, $type) = getImageSize($file_src);

	switch ($type) {
		case 1: // gif -> jpg
			$img_src = imagecreatefromgif($file_src);
			break;
		case 2: // jpeg -> jpg
			$img_src = imagecreatefromjpeg($file_src);
			break;
		case 3: // png -> jpg
			$img_src = imagecreatefrompng($file_src);
			break;
	}

	$thumb = 100;
	if ($w_src > $h_src) {
		$w_dst = $thumb;
		$h_dst = round($thumb / $w_src * $h_src);
	} else {
		$w_dst = round($thumb / $w_src * $h_src);
		$h_dst = $thumb;
	}

	$img_dst = imagecreatetruecolor($w_dst, $h_dst);

	imagecopyresampled(
		$img_dst,
		$img_src,
		0,
		0,
		0,
		0,
		$w_dst,
		$h_dst,
		$w_src,
		$h_src
	);
	imagejpeg($img_dst, $file_dst);
	imagedestroy($img_src);
	imagedestroy($img_dst);
}
?>
<script language="JavaScript">
            alert('Anda Berhasil Menambah Data');
            window.location='index.php?page=data_soal';
        </script>
</center>
