<?php
$kon = new mysqli("localhost", "root", "", "ujianamin");

$id_soal = $_GET['id_soal'];

$query = mysqli_query($kon, "select soal.*, guru.*, kelas.*, mapel.*
from soal
join guru on soal.nip=guru.nip
join kelas on soal.id_kelas=kelas.id_kelas
join mapel on soal.id_mapel=mapel.id_mapel
where id_soal='$id_soal'");

$data = mysqli_fetch_array($query);

?>


<div class="row">
  <div class="col-md-8">
    <div class="box box-warning">
      <div class="box-header with-border">
        <h3 class="box-title">Isi Data Soal</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <form role="form" action="?page=proses_edit_soal" method="post" enctype="multipart/form-data">
          <input type="hidden" class="form-control" name="id_soal" value="<?php echo $data['id_soal']; ?>">
          <!-- text input -->
          <div class="form-group">
            <label>Uraian Soal</label>
            <textarea class="form-control" id="editor1" rows="15" name="nama_soal"><?php echo $data['nama_soal']; ?></textarea>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Jawaban Huruf A</label>
                <textarea class="textarea" id="editor1" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" rows="5" name="a"><?php echo $data['a']; ?></textarea>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Jawaban Huruf B</label>
                <textarea class="textarea" id="editor1" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" rows="5" name="b"><?php echo $data['b']; ?></textarea>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Jawaban Huruf C</label>
                <textarea class="textarea" id="editor1" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" rows="5" name="c"><?php echo $data['c']; ?></textarea>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Jawaban Huruf D</label>
                <textarea class="textarea" id="editor1" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" rows="5" name="d"><?php echo $data['d']; ?></textarea>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label>Nama Mapel</label>
            <select class="form-control" name="id_mapel">
              <option value="<?php echo $data['id_mapel']; ?>" selected="selected"><?php echo $data['nama_mapel']; ?> (Dipilih)</option>
              <?php
              require_once '../adminis/pengaturan.php';
              $stmt = $db->prepare("SELECT * FROM mapel WHERE nip='$_SESSION[nip]'");
              $stmt->execute();
              ?>
              <?php while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) : ?>
                <?php extract($row); ?>
                <option value="<?php echo $row['id_mapel']; ?>">
                  <?php echo $row['nama_mapel']; ?>
                </option>
              <?php endwhile; ?>
            </select>
          </div>
          <div class="form-group">
            <label>Nama Kelas</label>
            <select class="form-control" name="id_kelas">
              <option value="<?php echo $data['id_kelas']; ?>" selected="selected"><?php echo $data['nama_kelas']; ?> <?php echo $data['abjad_kelas']; ?></option>
              <?php
              require_once '../adminis/pengaturan.php';
              $stmt = $db->prepare("SELECT mapel.id_kelas, kelas.nama_kelas, kelas.abjad_kelas
                                                            FROM mapel
                                                            JOIN kelas ON mapel.id_kelas=kelas.id_kelas 
                                                            WHERE mapel.nip='$_SESSION[nip]'");
              $stmt->execute();
              ?>
              <?php while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) : ?>
                <?php extract($row); ?>
                <option value="<?php echo $row['id_kelas']; ?>">
                  <?php echo $row['nama_kelas']; ?> <?php echo $row['abjad_kelas']; ?>
                </option>
              <?php endwhile; ?>
            </select>
          </div>
          <div class="form-group">
            <label>NIP Guru</label>
            <input type="text" class="form-control" name="nip" value="<?php echo $data['nip']; ?>" readonly>
          </div>
          <div class="form-group">
            <label>Kunci Jawaban</label>
            <input type="text" class="form-control" name="kunci_jawaban" value="<?php echo $data['kunci_jawaban']; ?>">
          </div>
          <div class="form-group">
            <label for="exampleInputFile">Gambar Soal</label>
            <input type="file" id="exampleInputFile" name="gambar" value="<?php echo $data['gambar']; ?>">
            <p class="help-block">Masukkan gambar jika ada.</p>
          </div>
          <div class="form-group">
            <label>Aktif</label>
            <select class="form-control" name="aktif">
              <?php
              if ($data['aktif'] == "Y") {
                $jekel = "<option value='N'>TIDAK</option>";
                $kata = "YA";
              } else {
                $jekel = "<option value='Y'>YA</option>";
                $kata = "TIDAK";
              }
              ?>
              <option value="<?php echo $data['aktif']; ?>"><?php echo $kata; ?></option>
              <?php echo $jekel; ?>
            </select>
          </div>
          <div class="form-group">
            <button type="submit" name="ubah" class="btn btn-success">Ubah Data</button>
          </div>


        </form>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
  <div class="col-md-4">
    <div class="box box-warning">
      <div class="box-header with-border">
        <h3 class="box-title"><b>Tips!</b></h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse">
            <p>Saya Mengerti/Saya kurang paham!</p></i>
          </button>
        </div>
        <!-- /.box-tools -->
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <b>Random Password</b>: Digunakan untuk me-generate password tanpa harus mengetik password untuk guru.<br />
        Isikan Data Guru sesuai pendaftarannya. NIP guru akan otomatis mengikuti urutan kapan data
        guru di input.
      </div>
      <!-- /.box-body -->
    </div>
  </div>
</div>