<div class="box">
  <div class="box-header">
    <h3 class="Data Kelas">Data nilai perkelas</h3>
    <h5 class="Data Kelas">Pilih Kelas </h5>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="row">

      <?php
      require '../function/kon.php';

      $kelas = mysqli_query($kon, "SELECT kelas.*
	  FROM kelas
	  JOIN siswa ON kelas.id_kelas = siswa.id_kelas 
	  JOIN mapel ON kelas.id_kelas = mapel.id_kelas
	  JOIN guru ON mapel.nip = guru.nip
	  JOIN ujian ON ujian.id_mapel = mapel.id_mapel
	  WHERE ujian.nip = '$_SESSION[nip]' GROUP BY kelas.id_kelas");
      while ($data_kelas = mysqli_fetch_array($kelas)) {
        ?>
      <div class="col-md-4">
        <div class="small-box bg-blue">
          <div class="inner">
            <h3>Kelas <?php echo $data_kelas['nama_kelas']; ?>-<?php echo $data_kelas['abjad_kelas']; ?></h3>
          </div>
          <div class="icon">
            <i class="ion ion-book"></i>
          </div>
          <a href="?page=lihat_nilai_per_mapel&kelas=<?php echo $data_kelas['id_kelas']; ?>" class="btn btn-success btn-block">
            Lihat nilai
          </a>
        </div>
      </div>
      <?php } ?>
    </div>
  </div>
  <!-- /.box-body -->
</div>