<div class="box">
    <div class="box-header">
        <h3 class="Data Kelas">Data nilai di kelas Anda. </h3>
        <h5 class="Data Kelas"></h5>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">

            <?php
            require '../function/kon.php';

            $guru = mysqli_query($kon, "SELECT * FROM guru WHERE nip = '$_SESSION[nip]'");
            $teacher = mysqli_fetch_array($guru);
            require '../function/kon.php';
            $kelas = $_GET['kelas'];
            
            $mapel = mysqli_query($kon, "SELECT siswa.*, mapel.*, kelas.*, guru.*, ujian.*
                                        FROM kelas
                                        JOIN siswa ON kelas.id_kelas = siswa.id_kelas 
                                        JOIN mapel ON kelas.id_kelas = mapel.id_kelas
                                        JOIN guru ON mapel.nip = guru.nip
                                        JOIN ujian ON ujian.id_mapel = mapel.id_mapel
                                        WHERE ujian.nip = '$teacher[nip]' AND kelas.id_kelas='$kelas'
                                        GROUP BY mapel.id_mapel");
            while ($data_mapel = mysqli_fetch_array($mapel)) {
                ?>
            <div class="col-md-4">
                <div class="small-box bg-blue">
                    <div class="inner">
                        <h3><?php echo $data_mapel['nama_mapel']; ?></h3>
                        <p>Kelas <?php echo $data_mapel['nama_kelas']; ?>-<?php echo $data_mapel['abjad_kelas']; ?>
                    </div>
                    <div class="icon">
                    <i class="ion ion-book"></i>
                    </div>
                    <a href="?page=lihat_nilai_per_kelas&kelas=<?php echo $data_mapel['id_kelas']; ?>&mapel=<?php echo $data_mapel['id_mapel']; ?>" class="btn btn-success btn-block">
                        Lihat nilai
                    </a>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
    <!-- /.box-body -->
</div>