<div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">

              <h3 class="profile-username text-center"><b><?php echo $_SESSION['nama_guru']; ?>, <?php echo $_SESSION['gelar']; ?></b></h3>

              <p class="text-muted text-center"><?php echo $_SESSION['nip']; ?> - Guru</p>

              <p class="text-muted text-center"><?php echo $_SESSION['alamat']; ?></p>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">About Me</h3>
            </div>
            box-header
            <div class="box-body">
              <strong><i class="fa fa-book margin-r-5"></i> Education</strong>

              <p class="text-muted">
                B.S. in Computer Science from the University of Tennessee at Knoxville
              </p>

              <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

              <p class="text-muted">Malibu, California</p>

              <hr>

              <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>

              <p>
                <span class="label label-danger">UI Design</span>
                <span class="label label-success">Coding</span>
                <span class="label label-info">Javascript</span>
                <span class="label label-warning">PHP</span>
                <span class="label label-primary">Node.js</span>
              </p>

              <hr>

              <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
            </div>
            box-body
          </div>
          box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <!-- <li class="active"><a href="#activity" data-toggle="tab">Activity</a></li>
              <li><a href="#timeline" data-toggle="tab">Timeline</a></li> -->
              <li ><a href="#settings" data-toggle="tab">Pengaturan Akun</a></li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="settings">
                <form class="form-horizontal" action="?page=update_profil" method="post">
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Nama Pengguna</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputName" name="nip" value="<?php echo $_SESSION['nip']; ?>" readonly>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Kata Sandi</label>
                    <div class="col-sm-10">
                      <input type="password" class="form-control" id="inputPassword" name="password" value="<?php echo $_SESSION['password']; ?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Nama Anda</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputName" name="nama_guru" value="<?php echo $_SESSION['nama_guru']; ?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputExperience" class="col-sm-2 control-label">Alamat</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" name="alamat" id="inputExperience"><?php echo $_SESSION['alamat']; ?></textarea>
                    </div>
                  </div>

                  <input type="hidden" class="form-control" id="inputName" name="jk" value="<?php echo $_SESSION['jk']; ?>">

                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Nomor Telepon/HP</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputName" name="no_telp" value="<?php echo $_SESSION['no_telp']; ?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Gelar Pendidikan</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputName" name="gelar" value="<?php echo $_SESSION['gelar']; ?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-success">Ubah Profil</button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>