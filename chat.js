//create firebase reference
var dbRef = new Firebase("https://chat-amin-d63a3.firebaseio.com/");
var chatsRef = dbRef.child('chats')

//load older conatcts as well as any newly added one...
chatsRef.on("child_added", function(snap) {
  console.log("added", snap.key(), snap.val());
  document.querySelector('#message_box').innerHTML += (chatHtmlFromObject(snap.val()));
});

//save chat
document.querySelector('#save').addEventListener("click", function(event) {
  var a = new Date(),
  b = a.getDate(),
  c = a.getMonth(),
  d = a.getFullYear(),
  date = b + '-' + c + '-' + d,
     chatForm = document.querySelector('#msg_form');
  event.preventDefault();
  if (document.querySelector('#name').value != '' && document.querySelector('#message').value != '') {
  chatsRef
  .push({
  name: document.querySelector('#name').value,
  message: document.querySelector('#message').value,
  date: date
  })
  chatForm.reset();
  } else {
  alert('Harap memasukkan pesan terlebih dahulu!');
  }
}, false);

//prepare conatct object's HTML
function chatHtmlFromObject(chat) {
  console.log(chat);
   var bubble = (chat.name == document.querySelector('#name').value ? "bubble-right" : "bubble-left");
  var html = '<div class="' + bubble + '"><p><b><span class="name">' + chat.name + '</span></b><br/><span class="msgc">' + chat.message + '</span><br/><br/><font size="2"><span class="date">' + chat.date + '</span></font></p></div>';
  return html;
}