<!DOCTYPE html>
<html>
<head>
 <title>BELAJAR AJAX</title>
 <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>   <!-- INCLUDE jQuery -->
</head>
<body>
<form action="#" method="post">
 <input type="text" name="npm" id="npm" placeholder="NPM">
 <input type="text" name="nama" id="nama" placeholder="Nama">
 <input type="submit" value="simpan">
</form>

<script type="text/javascript">
$(document).ready(function(){

 $('#npm').change(function(){    // KETIKA ISI DARI FIEL 'NPM' BERUBAH MAKA ......
  var npmfromfield = $('#npm').val();  // AMBIL isi dari fiel NPM masukkan variabel 'npmfromfield'
  $.ajax({        // Memulai ajax
    method: "POST",      
    url: "ajaxrespon.php",    // file PHP yang akan merespon ajax
    data: { npm: npmfromfield}   // data POST yang akan dikirim
  })
    .done(function( hasilajax ) {   // KETIKA PROSES Ajax Request Selesai
        $('#nama').val(hasilajax);  // Isikan hasil dari ajak ke field 'nama' 
    });
 })
});
</script>
</body>
</html>