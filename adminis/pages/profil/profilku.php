<div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">

              <h3 class="profile-username text-center"><b><?php echo $_SESSION['nam_ad']; ?></b></h3>

              <p class="text-muted text-center"><?php echo $_SESSION['username']; ?> - Tata Usaha</p>
            </div>
            <!-- /.box-body -->
          </div>
          
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <!-- <li class="active"><a href="#activity" data-toggle="tab">Activity</a></li>
              <li><a href="#timeline" data-toggle="tab">Timeline</a></li> -->
              <li ><a href="#settings" data-toggle="tab">Pengaturan Akun</a></li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="settings">
                <form class="form-horizontal" action="?page=proses_edit_user" method="POST">
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Nama Pengguna</label>
                    <input type="hidden" class="form-control" name="id_ad" id="inputName" value="<?php echo $_SESSION['id_ad']; ?>">
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputName" name="username" value="<?php echo $_SESSION['username']; ?>" readonly>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Kata Sandi</label>

                    <div class="col-sm-10">
                      <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Masukkan Kata Sandi Baru">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Nama Lengkap</label>

                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="nam_ad" id="inputName" value="<?php echo $_SESSION['nam_ad']; ?>">
                    </div>
                  </div>
                  <input type="hidden" class="form-control" name="jk" id="inputName" value="<?php echo $_SESSION['jk']; ?>">
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-success">Ubah Profil</button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>