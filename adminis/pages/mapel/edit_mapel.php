<?php
$kon = new mysqli("localhost", "root", "", "ujianamin");

$id_mapel = $_GET['id_mapel'];

$query = mysqli_query($kon, "select mapel.*, guru.*, kelas.* from mapel
join guru
on mapel.nip=guru.nip
join kelas on mapel.id_kelas=kelas.id_kelas
where id_mapel='$id_mapel'") or die(mysqli_error());

$data = mysqli_fetch_array($query);

?>

<div class="row">
    <div class="col-md-6">
<div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Data Mapel</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" action="pages/mapel/proses_edit.php" method="post">
                <!-- text input -->
                <input type="hidden" class="form-control" name="id_mapel" value="<?php echo $data['id_mapel']; ?>">
                <div class="form-group">
                  <label>Nama Mapel</label>
                  <input type="text" class="form-control" name="nama_mapel" value="<?php echo $data['nama_mapel']; ?>">
                </div>
                <div class="form-group">
                  <label>Nama Guru</label>
                  <select class="form-control" name="nip">
                  <option value="<?php echo $data['nip']; ?>" selected="selected"><?php echo $data['nama_guru']; ?>, <?php echo $data['gelar']; ?></option>
                                            <?php
                                                            require_once 'pengaturan.php';
                                                            $stmt = $db->prepare("SELECT * FROM guru where nip not in ($data[nip])");
                                                            $stmt->execute();
                                                            ?>
                                            <?php while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) : ?>
                                            <?php extract($row); ?>
                                            <option value="<?php echo $row['nip']; ?>">
                                                <?php echo $row['nama_guru']; ?>, <?php echo $row['gelar']; ?>
                                            </option>
                                            <?php endwhile; ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Nama Kelas</label>
                  <select class="form-control" name="id_kelas">
                  <option value="<?php echo $data['id_kelas']; ?>" selected="selected"><?php echo $data['nama_kelas']; ?> <?php echo $data['abjad_kelas']; ?></option>
                                            <?php
                                                            require_once 'pengaturan.php';
                                                            $stmt = $db->prepare("SELECT * FROM kelas where id_kelas not in ($data[id_kelas])");
                                                            $stmt->execute();
                                                            ?>
                                            <?php while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) : ?>
                                            <?php extract($row); ?>
                                            <option value="<?php echo $row['id_kelas']; ?>">
                                                <?php echo $row['nama_kelas']; ?> <?php echo $row['abjad_kelas']; ?>
                                            </option>
                                            <?php endwhile; ?>
                  </select>
                </div>
                <div class="form-group">
                <button type="submit" class="btn btn-success">Ubah Data</button>
                </div>


              </form>
            </div>
            <!-- /.box-body -->
          </div>
</div>
<div class="col-md-6">
<div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title"><b>Tips!</b></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><p>Saya Mengerti/Saya kurang paham!</p></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <b>Random Password</b>: Digunakan untuk me-generate password tanpa harus mengetik password untuk guru.<br/>
              Isikan Data Guru sesuai pendaftarannya. NIP guru akan otomatis mengikuti urutan kapan data
              guru di input.
            </div>
            <!-- /.box-body -->
          </div>
</div>
</div>