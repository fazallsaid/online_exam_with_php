<?php require 'randompass.php'; ?>

<div class="row">
    <div class="col-md-6">
<div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Isi Data Mapel</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" action="pages/mapel/proses_tambah.php" method="post">
                <!-- text input -->
                <div class="form-group">
                  <label>Nama Mapel</label>
                  <input type="text" class="form-control" name="nama_mapel" placeholder="Masukkan Nama Mapel ...">
                </div>
                <div class="form-group">
                  <label>Nama Guru</label>
                  <select class="form-control" name="nip">
                  <option value="show-all" selected="selected">= Pilih Guru =</option>
                                            <?php
                                                            require_once 'pengaturan.php';
                                                            $stmt = $db->prepare('SELECT * FROM guru');
                                                            $stmt->execute();
                                                            ?>
                                            <?php while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) : ?>
                                            <?php extract($row); ?>
                                            <option value="<?php echo $row['nip']; ?>">
                                                <?php echo $row['nama_guru']; ?>, <?php echo $row['gelar']; ?>
                                            </option>
                                            <?php endwhile; ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Nama Kelas</label>
                  <select class="form-control" name="id_kelas">
                  <option value="show-all" selected="selected">= Pilih Kelas =</option>
                                            <?php
                                                            require_once 'pengaturan.php';
                                                            $stmt = $db->prepare('SELECT * FROM kelas');
                                                            $stmt->execute();
                                                            ?>
                                            <?php while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) : ?>
                                            <?php extract($row); ?>
                                            <option value="<?php echo $row['id_kelas']; ?>">
                                                <?php echo $row['nama_kelas']; ?> <?php echo $row['abjad_kelas']; ?>
                                            </option>
                                            <?php endwhile; ?>
                  </select>
                </div>
                <div class="form-group">
                <button type="submit" class="btn btn-success">Simpan</button>
                </div>


              </form>
            </div>
            <!-- /.box-body -->
          </div>
</div>
<div class="col-md-6">
<div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title"><b>Tips!</b></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><p>Saya Mengerti/Saya kurang paham!</p></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              Isikan data Mata Pelajaran yang sesuai. Serta masukkan guru yang akan mengampu mata pelajaran tersebut.
            </div>
            <!-- /.box-body -->
          </div>
</div>
</div>