

<?php require 'randompass.php'; ?>

<?php
$kon = new mysqli("localhost", "root", "", "ujianamin");

$nip = $_GET['nip'];

$query = mysqli_query($kon, "select * from guru where nip='$nip'") or die(mysqli_error());

$data = mysqli_fetch_array($query);

?>

<div class="row">
    <div class="col-md-6">
<div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Data Guru</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" action="pages/guru/proses_edit.php" method="post">
              <input type="hidden" class="form-control" name="nip" value="<?php echo $data['nip']; ?>">
                <!-- text input -->
                <div class="form-group">
                  <label>Kata Sandi</label>
                  <input type="password" class="form-control" name="password" value="<?php echo $data['password']; ?>">
                </div>
                <div class="form-group">
                  <label>Nama Guru</label>
                  <input type="text" class="form-control" name="nama_guru" value="<?php echo $data['nama_guru']; ?>">
                </div>
                <div class="form-group">
                  <label>Alamat</label>
                  <textarea class="form-control" rows="3" name="alamat"><?php echo $data['alamat']; ?></textarea>
                </div>
                <div class="form-group">
                  <label>Jenis Kelamin</label>
                  <select class="form-control" name="jk">
                  <option value="<?php echo $data['jk']; ?>"><?php echo $data['jk']; ?></option>
                  <?php 
                  if ($data['jk']=="Pria"){
                    $jekel = "<option value='Wanita'>Wanita</option>";
                  }else{
                    $jekel = "<option value='Pria'>Pria</option>";
                  }
                  ?>
                  <?php echo $jekel; ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>No Telepon/HP</label>
                  <input type="text" class="form-control" name="no_telp" value="<?php echo $data['no_telp']; ?>">
                </div>
                <div class="form-group">
                  <label>Gelar</label>
                  <input type="text" class="form-control" name="gelar" value="<?php echo $data['gelar']; ?>">
                </div>
                <div class="form-group">
                <button type="submit" class="btn btn-success">Simpan</button>
                </div>


              </form>
            </div>
            <!-- /.box-body -->
          </div>
</div>
<div class="col-md-6">
<div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title"><b>Tips!</b></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><p>Saya Mengerti/Saya kurang paham!</p></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <b>Random Password</b>: Digunakan untuk me-generate password tanpa harus mengetik password untuk guru.<br/>
              Isikan Data Guru sesuai pendaftarannya. NIP guru akan otomatis mengikuti urutan kapan data
              guru di input.
            </div>
            <!-- /.box-body -->
          </div>
</div>
</div>