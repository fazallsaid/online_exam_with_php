<?php require 'randompass.php'; ?>

<div class="row">
    <div class="col-md-6">
<div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Isi Data Guru</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" action="pages/guru/proses_tambah.php" method="post">
                <!-- text input -->
                <div class="form-group">
                  <label>Kata Sandi</label>
                  <input type="password" class="form-control" name="password" value="<?php echo passAcak(8);?>" readonly>
                </div>
                <div class="form-group">
                  <label>Nama Guru</label>
                  <input type="text" class="form-control" name="nama_guru" placeholder="Masukkan Nama Guru ...">
                </div>
                <div class="form-group">
                  <label>Alamat</label>
                  <textarea class="form-control" rows="3" name="alamat" placeholder="Masukkan Alamat ..."></textarea>
                </div>
                <div class="form-group">
                  <label>Jenis Kelamin</label>
                  <select class="form-control" name="jk">
                  <option value="">== Pilih Jenis Kelamin ==</option>
                  <option value="Pria">Pria</option>
                  <option value="Wanita">Wanita</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>No Telepon/HP</label>
                  <input type="text" class="form-control" name="no_telp" placeholder="Masukkan Nomor Telepon/HP ...">
                </div>
                <div class="form-group">
                  <label>Gelar</label>
                  <select class="form-control" name="gelar">
                  <option value="">== Pilih Gelar ==</option>
                  <option value="S.Ag">S.Ag</option>
                  <option value="S.Pd">S.Pd</option>
                  <option value="S.Kom">S.Kom</option>
                  <option value="S.T">S.T</option>
                  <option value="S.H">S.H</option>
                  <option value="S.E">S.E</option>
                  <option value="S.Sos">S.Sos</option>
                  </select>
                </div>
                <div class="form-group">
                <button type="submit" class="btn btn-success">Simpan</button>
                </div>


              </form>
            </div>
            <!-- /.box-body -->
          </div>
</div>
<div class="col-md-6">
<div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title"><b>Tips!</b></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><p>Saya Mengerti/Saya kurang paham!</p></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <b>Random Password</b>: Digunakan untuk me-generate password tanpa harus mengetik password untuk guru.<br/>
              Isikan Data Guru sesuai pendaftarannya. NIP guru akan otomatis mengikuti urutan kapan data
              guru di input.
            </div>
            <!-- /.box-body -->
          </div>
</div>
</div>