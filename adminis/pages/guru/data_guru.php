<?php
include "config.php";
$query = mysqli_query($connection, "select * from guru");
?>

<div class="box">
            <div class="box-header">
              <h3 class="Data Guru">Data Guru</h3> <a type="button" class="btn btn-success" href="./?page=tambah_guru">Tambah Data Guru</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>NIP</th>
                  <th>Nama Guru</th>
                  <th>Alamat</th>
                  <th>No Telp</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                <?php if (mysqli_num_rows($query) > 0) { ?>
                                    <?php
                                    $no = 1;
                                    while ($data = mysqli_fetch_array($query)) {
                                        ?>
                                    <tr>
                                        <!-- <td></td> -->
                                        <td><?php echo $no ?></td>
                                        <td><font face="trebuchet MS"><?php echo $data["nip"]; ?></font></td>
                                        <td><font face="trebuchet MS"><?php echo $data["nama_guru"]; ?>, <?php echo $data["gelar"]; ?></font></td>
                                        <td><font face="trebuchet MS"><?php echo $data["alamat"]; ?></font></td>
										                    <td><font face="trebuchet MS"><?php echo $data["no_telp"]; ?></font></td>
                                        <td>
                                        <a href="pages/guru/aksi_print.php?nip=<?php echo $data["nip"]; ?>&&jk=<?php echo $tata_usaha; ?>&&nama_ad=<?php echo $_SESSION["nam_ad"]; ?>"><i class="fa fa-print edu-checked-pro" aria-hidden="true" style="color: blue; font-size: 15px"></i></a> |
                                        <a href="?page=edit_guru&nip=<?php echo $data['nip']; ?>" ><i class="fa fa-edit edu-checked-pro" aria-hidden="true" style="color: green; font-size: 15px"></i></a> |
                                        <a href="pages/guru/hapus_guru.php?nip=<?php echo $data['nip']; ?>" onClick="return confirm('Apakah anda yakin untuk menghapus data ini? (<?php echo $data['nip']; ?>)');"  ><i class="fa fa-trash edu-checked-pro" aria-hidden="true" style="color: red; font-size: 15px"></i></a>
                                        </td>
                                    </tr>
                                            <?php 
                                            $no++;
                                        }
                                    }
                                    ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>