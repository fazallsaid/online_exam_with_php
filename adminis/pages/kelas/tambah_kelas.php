<?php require 'randompass.php'; ?>

<div class="row">
    <div class="col-md-6">
<div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Isi Data Kelas</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" action="pages/kelas/proses_tambah.php" method="post">
                <!-- text input -->
                <div class="form-group">
                  <label>Nama Kelas</label>
                  <input type="text" class="form-control" name="nama_kelas" placeholder="Masukkan Nama Kelas (mis: X, XI, XII) ...">
                </div>
                <div class="form-group">
                  <label>Abjad Kelas</label>
                  <select class="form-control" name="abjad_kelas">
                  <option value="A">A</option>
                  <option value="B">B</option>
                  <option value="C">C</option>
                  <option value="D">D</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Jumlah Siswa</label>
                  <input type="number" class="form-control" name="jml_siswa" placeholder="Masukkan jumlah siswa ...">
                </div>
                <div class="form-group">
                <button type="submit" class="btn btn-success">Simpan</button>
                </div>


              </form>
            </div>
            <!-- /.box-body -->
          </div>
</div>
<div class="col-md-6">
<div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title"><b>Tips!</b></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><p>Saya Mengerti/Saya kurang paham!</p></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              Isikan data kelas sesuai kelas yang ada. <br/>
              <b>Nama Kelas</b> : Adalah kelas berapa yang akan di inputkan.<br/>
              <b>Abjad Kelas</b> : Adalah abjad dari masing - masing kelas.
            </div>
            <!-- /.box-body -->
          </div>
</div>
</div>