<?php
include "config.php";
$query = mysqli_query($connection, "SELECT * FROM kelas");
?>

<div class="box">
            <div class="box-header">
              <h3 class="Data Kelas">Data Kelas</h3> <a type="button" class="btn btn-success" href="./?page=tambah_kelas">Tambah Data Kelas</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Kelas</th>
                  <th>Jumlah Siswa</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                <?php if (mysqli_num_rows($query) > 0) { ?>
                                    <?php
                                    $no = 1;
                                    while ($data = mysqli_fetch_array($query)) {
                                        ?>
                                    <tr>
                                        <!-- <td></td> -->
                                        <td><?php echo $no ?></td>
                                        <td><font face="trebuchet MS">Kelas <?php echo $data["nama_kelas"]; ?> <?php echo $data["abjad_kelas"]; ?></font></td>
                                        <td><font face="trebuchet MS"><?php echo $data["jml_siswa"]; ?> Siswa</font></td>
                                        <td>
                                        <a href="?page=edit_kelas&id_kelas=<?php echo $data['id_kelas']; ?>" ><i class="fa fa-edit edu-checked-pro" aria-hidden="true" style="color: green; font-size: 15px"></i></a> |
                                        <a href="pages/kelas/hapus_kelas.php?id_kelas=<?php echo $data['id_kelas']; ?>" onClick="return confirm('Apakah anda yakin untuk menghapus data ini?');"  ><i class="fa fa-trash edu-checked-pro" aria-hidden="true" style="color: red; font-size: 15px"></i></a>
                                        </td>
                                    </tr>
                                            <?php 
                                            $no++;
                                        }
                                    }
                                    ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>