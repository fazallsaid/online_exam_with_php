<?php
$kon = new mysqli("localhost", "root", "", "ujianamin");

$id_kelas = $_GET['id_kelas'];

$query = mysqli_query($kon, "select * from kelas where id_kelas='$id_kelas'") or die(mysqli_error());

$data = mysqli_fetch_array($query);

?>

<div class="row">
    <div class="col-md-6">
<div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Data Kelas</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" action="pages/kelas/proses_edit.php" method="post">
              <input type="hidden" class="form-control" name="id_kelas" value="<?php echo $data['id_kelas']; ?>">
                <!-- text input -->
                <div class="form-group">
                  <label>Nama Kelas</label>
                  <input type="text" class="form-control" name="nama_kelas" value="<?php echo $data['nama_kelas']; ?>">
                </div>
                <div class="form-group">
                  <label>Abjad Kelas</label>
                  <select class="form-control" name="abjad_kelas">
                  <option value="<?php echo $data['abjad_kelas']; ?>"><?php echo $data['abjad_kelas']; ?></option>
                  <?php 
                  if ($data['abjad_kelas']=="A"){
                    $abjad = "<option value='B'>B</option>";
                    $abjad = "<option value='C'>C</option>";
                    $abjad = "<option value='D'>D</option>";
                  }elseif ($data['abjad_kelas']=="B"){
                    $abjad = "<option value='A'>A</option>";
                    $abjad = "<option value='C'>C</option>";
                    $abjad = "<option value='D'>D</option>";
                  }elseif ($data['abjad_kelas']=="C"){
                    $abjad = "<option value='A'>A</option>";
                    $abjad = "<option value='B'>B</option>";
                    $abjad = "<option value='D'>D</option>";
                  }else{
                    $abjad = "<option value='A'>A</option>";
                    $abjad = "<option value='B'>B</option>";
                    $abjad = "<option value='C'>C</option>";
                  }
                  ?>
                  <?php echo $abjad; ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Jumlah Siswa</label>
                  <input type="number" class="form-control" name="jml_siswa" value="<?php echo $data['jml_siswa']; ?>">
                </div>
                <div class="form-group">
                <button type="submit" class="btn btn-success">Simpan</button>
                </div>


              </form>
            </div>
            <!-- /.box-body -->
          </div>
</div>
<div class="col-md-6">
<div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title"><b>Tips!</b></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><p>Saya Mengerti/Saya kurang paham!</p></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <b>Random Password</b>: Digunakan untuk me-generate password tanpa harus mengetik password untuk guru.<br/>
              Isikan Data Guru sesuai pendaftarannya. NIP guru akan otomatis mengikuti urutan kapan data
              guru di input.
            </div>
            <!-- /.box-body -->
          </div>
</div>
</div>