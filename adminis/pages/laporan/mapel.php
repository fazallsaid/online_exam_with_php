<div class="box">
    <div class="box-header">
        <h3 class="Data Kelas">Laporan Per Mata Pelajaran</h3>
        <h5 class="Data Kelas">Pilih Mata Pelajaran Yang Ingin Dilihat Laporannya</h5>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">

            <?php
            require '../function/kon.php';

            $tu = mysqli_query($kon, "SELECT * FROM tu_admin WHERE nam_ad = '$_SESSION[nam_ad]'");
            $admin = mysqli_fetch_array($tu);

            $kelas = $_GET['kelas'];
            $mapel = mysqli_query($kon, "SELECT siswa.*, mapel.*, kelas.*, guru.*, ujian.*
                                        FROM kelas
                                        JOIN siswa ON kelas.id_kelas = siswa.id_kelas 
                                        JOIN mapel ON kelas.id_kelas = mapel.id_kelas
                                        JOIN guru ON mapel.nip = guru.nip
                                        JOIN ujian ON ujian.id_mapel = mapel.id_mapel
                                        WHERE kelas.id_kelas = '$kelas'
                                        AND ujian.id_mapel = mapel.id_mapel
                                        GROUP BY mapel.nama_mapel");
            while ($data_mapel = mysqli_fetch_array($mapel)) {
                ?>
            <div class="col-md-4">
                <div class="small-box bg-blue">
                    <div class="inner">
                        <h3><?php echo $data_mapel['nama_mapel']; ?></h3>
                        <p>
                            Jumlah Siswa : <?php echo $data_mapel['jml_siswa']; ?><br />
                        </p>
                    </div>
                    <div class="icon">
                        <a href="pages/laporan/aksi_print.php?jk=<?php echo $admin['jk']; ?>&&nama_ad=<?php echo $_SESSION["nam_ad"]; ?>&&mapel=<?php echo $data_mapel['id_mapel']; ?>&&kelas=<?php echo $kelas; ?>" class="btn btn-success">Cetak</a>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
    <!-- /.box-body -->
</div>