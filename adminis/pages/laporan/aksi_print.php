<?php
$logo = "<img src='../../dist/img/man_sintang1.png' width='30' height='30'/>";
$tu = $_GET['nama_ad'];
$tu1 = $_GET['jk'];
$id_mapel = $_GET['mapel'];
$id_kelas = $_GET['kelas'];

if($tu1=="Pria"){
  $j = "Bpk. ";
}else{
  $j = "Ib. ";
}

include "../../config.php";
$mapel = mysqli_query($connection, "SELECT *
                                  FROM mapel 
                                  WHERE id_mapel = '$id_mapel'");
$data_mapel = mysqli_fetch_array($mapel);

$sqlKelas = mysqli_query($connection, "SELECT *
                                  FROM kelas 
                                  WHERE id_kelas = '$id_kelas'");
$data_kelas = mysqli_fetch_array($sqlKelas);

$query = mysqli_query($connection, "SELECT kelas.*, mapel.* ,ujian.nis, ujian.tgl_ujian, nama_siswa, MAX(nilai) as score
                                  FROM ujian 
                                  JOIN siswa ON siswa.nis=ujian.nis
                                  JOIN mapel ON mapel.id_mapel=ujian.id_mapel
                                  JOIN kelas ON kelas.id_kelas = mapel.id_kelas
                                  WHERE ujian.id_mapel = '$id_mapel'
                                  GROUP BY nama_siswa ORDER BY nis ASC");
$bulan_ini = date('n');
$tanggal = date('l, d-m-Y');
?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Print Rekap Nilai Siswa</title>

  <link rel="shortcut icon" href="../../dist/img/man_sintang.jpg">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body onload="window.print();">
  <div class="wrapper">
    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <?php echo $logo; ?> MAN Sintang
            <small class="pull-right">Tanggal: <?php echo $tanggal; ?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          Dari
          <address>
            <strong>MAN Sintang</strong><br>
            Jl. Jalan<br>
            Kalimantan Barat
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          Kepada
          <address>
            <strong>Pegawai Tata Usaha <br>
              <?php echo $j; ?><?php echo $tu; ?></strong>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <?php
          echo "<b>Print Laporan <br/> Kelas: " . $data_kelas['nama_kelas'] ." " . $data_kelas['abjad_kelas'] . " <br/> Mata Pelajaran: " . $data_mapel['nama_mapel'] . "</b><br>";
          ?>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>NIS</th>
                <th>Nama Siswa</th>
                <th>Nilai Tertinggi</th>
                <th>Semester</th>
              </tr>
            </thead>
            <tbody>
              <?php if (mysqli_num_rows($query) > 0) { ?>
              <?php
                $no = 1;
                while ($data = mysqli_fetch_array($query)) {
                  ?>
              <tr>
                <!-- <td></td> -->
                <td><?php echo $no ?></td>
                <td>
                  <font face="trebuchet MS"><?php echo $data["nis"]; ?></font>
                </td>
                <td>
                  <font face="trebuchet MS"><?php echo $data["nama_siswa"]; ?></font>
                </td>
                <td>
                  <font face="trebuchet MS"><?php echo $data["score"]; ?></font>
                </td>
                <?php
                    $bulan = date('n');
                    $tanggal = $data["tgl_ujian"];
                    $date   =    date('m', strtotime($tanggal));
                    $b = ltrim($date, '0');
                    if ($b <= 6) {
                      $semester = "Genap";
                    } else {
                      $semester = "Ganjil";
                    }
                    ?>
                <td>
                  <font face="trebuchet MS"><?php echo $semester; ?></font>
                </td>
              </tr>
              <?php
                  $no++;
                }
              }
              ?>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            Mohon gunakan laporan ini sebaik - baiknya. Sekarang semester <?php echo $semester; ?>
          </p>
        </div>
        <!-- /.col -->
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- ./wrapper -->
</body>

</html>