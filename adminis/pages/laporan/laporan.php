<div class="box">
  <div class="box-header">
    <h3 class="Data Kelas">Laporan Per Kelas</h3>
    <h5 class="Data Kelas">Pilih Kelas </h5>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="row">

      <?php
      require '../function/kon.php';
      $kelas = mysqli_query($kon, "SELECT ujian.*, kelas.*
      FROM ujian
      JOIN mapel ON ujian.id_mapel=mapel.id_mapel
      JOIN kelas ON kelas.id_kelas=mapel.id_kelas");
      while ($data_kelas = mysqli_fetch_array($kelas)) {
        ?>
      <div class="col-md-4">
        <div class="small-box bg-blue">
          <div class="inner">
            <h3>Kelas <?php echo $data_kelas['nama_kelas']; ?>-<?php echo $data_kelas['abjad_kelas']; ?></h3>
            <p>
            <?php
            $quee = mysqli_query($kon, "SELECT COUNT(id_mapel) AS jml_mapel FROM mapel WHERE id_kelas='$data_kelas[id_kelas]'");
            $jum = mysqli_fetch_array($quee);
            ?>
              Jumlah Siswa : <?php echo $data_kelas['jml_siswa']; ?><br />
              Jumlah Mapel : <?php echo $jum['jml_mapel']; ?>
            </p>
          </div>
          <div class="icon">
            <i class="ion ion-book"></i>
          </div>
          <a href="?page=laporan_mapel&kelas=<?php echo $data_kelas['id_kelas']; ?>" class="btn btn-success btn-block">
            Lihat Laporan
          </a>
        </div>
      </div>
      <?php } ?>
    </div>
  </div>
  <!-- /.box-body -->
</div>