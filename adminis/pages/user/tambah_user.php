<?php require 'randompass.php'; ?>

<div class="row">
    <div class="col-md-6">
<div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Isi Data Admin</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" action="pages/user/proses_tambah.php" method="post">
                <!-- text input -->
                <div class="form-group">
                  <label>Nama Admin</label>
                  <input type="text" class="form-control" name="nam_ad" placeholder="Masukkan Nama Admin">
                </div>
                <div class="form-group">
                  <label>Username</label>
                  <input type="text" class="form-control" name="username" placeholder="Masukkan Username Anda">
                </div>
                <div class="form-group">
                  <label>Password</label>
                  <input type="text" class="form-control" name="password" placeholder="Masukkan Password Anda">
                </div>
                <div class="form-group">
                  <label>Jenis Kelamin</label>
                  <select class="form-control" name="jk">
                  <option value="Pria">Pria</option>
                  <option value="Wanita">Wanita</option>
                  </select>
                </div>
                <div class="form-group">
                <button type="submit" class="btn btn-success">Simpan</button>
                </div>


              </form>
            </div>
            <!-- /.box-body -->
          </div>
</div>
<div class="col-md-6">
<div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title"><b>Tips!</b></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><p>Saya Mengerti/Saya kurang paham!</p></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <b>Random Password</b>: Digunakan untuk me-generate password tanpa harus mengetik password untuk guru.<br/>
              Isikan Data Guru sesuai pendaftarannya. NIP guru akan otomatis mengikuti urutan kapan data
              guru di input.
            </div>
            <!-- /.box-body -->
          </div>
</div>
</div>