<?php require 'randompass.php'; ?>

<?php
$kon = new mysqli("localhost", "root", "", "ujianamin");

$nis = $_GET['nis'];

$query = mysqli_query($kon, "select * from siswa where nis='$nis'") or die(mysqli_error());

$data = mysqli_fetch_array($query);

?>

<div class="row">
    <div class="col-md-6">
<div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Ubah Data Siswa</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" action="pages/siswa/proses_edit.php" method="post">
              <input type="hidden" class="form-control" name="nis" value="<?php echo $data['nis']; ?>">
                <!-- text input -->
                <div class="form-group">
                  <label>Kata Sandi</label>
                  <input type="password" class="form-control" name="password" value="<?php echo $data['password']; ?>">
                </div>
                <div class="form-group">
                  <label>Nama Siswa</label>
                  <input type="text" class="form-control" name="nama_siswa" value="<?php echo $data['nama_siswa']; ?>">
                </div>
                <div class="form-group">
                  <label>Alamat</label>
                  <textarea class="form-control" rows="3" name="alamat"><?php echo $data['alamat']; ?></textarea>
                </div>
                <div class="form-group">
                  <label>Jenis Kelamin</label>
                  <select class="form-control" name="jk">
                  <option value="<?php echo $data['jk']; ?>"><?php echo $data['jk']; ?></option>
                  <?php 
                  if ($data['jk']=="Laki - Laki"){
                    $jekel = "<option value='Perempuan'>Perempuan</option>";
                  }else{
                    $jekel = "<option value='Laki - Laki'>Laki - Laki</option>";
                  }
                  ?>
                  <?php echo $jekel; ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Tempat Lahir</label>
                  <input type="text" class="form-control" name="tempat_lahir" value="<?php echo $data['tempat_lahir']; ?>">
                </div>
                <div class="form-group">
                  <label>Tanggal Lahir</label>
                  <input type="date" class="form-control" name="tgl_lahir" value="<?php echo $data['tgl_lahir']; ?>">
                </div>
                <div class="form-group">
                  <label>No Telepon/HP</label>
                  <input type="text" class="form-control" name="no_telp" value="<?php echo $data['no_telp']; ?>">
                </div>
                <div class="form-group">
                  <label>Asal Sekolah</label>
                  <input type="text" class="form-control" name="asal_sekolah" value="<?php echo $data['asal_sekolah']; ?>">
                </div>
                <div class="form-group">
                  <label>ID Kelas</label>
                  <input type="text" class="form-control" name="id_kelas" value="<?php echo $data['id_kelas']; ?>" readonly>
                </div>
                <div class="form-group">
                <button type="submit" class="btn btn-success">Ubah Data</button>
                </div>


              </form>
            </div>
            <!-- /.box-body -->
          </div>
</div>
<div class="col-md-6">
<div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title"><b>Tips!</b></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><p>Saya Mengerti/Saya kurang paham!</p></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              Kata Sandi untuk login siswa sudah tergenerate otomatis.<br/>
              Isikan Data Siswa sesuai pendaftarannya. NIS siswa akan otomatis mengikuti urutan kapan data
              siswa di input.
            </div>
            <!-- /.box-body -->
          </div>
</div>
</div>