<?php require 'randompass.php'; ?>

<div class="row">
  <div class="col-md-6">
    <div class="box box-warning">
      <div class="box-header with-border">
        <h3 class="box-title">Isi Data Siswa</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <form role="form" action="pages/siswa/proses_tambah.php" method="post">
          <!-- text input -->
          <div class="form-group">
            <label>Kata Sandi</label>
            <input type="password" class="form-control" name="password" value="<?php echo passAcak(8); ?>" readonly>
          </div>
          <div class="form-group">
            <label>Nama Siswa</label>
            <input type="text" class="form-control" name="nama_siswa" placeholder="Masukkan Nama Siswa ...">
          </div>
          <div class="form-group">
            <label>Alamat</label>
            <textarea class="form-control" rows="3" name="alamat" placeholder="Masukkan Alamat ..."></textarea>
          </div>
          <div class="form-group">
            <label>Jenis Kelamin</label>
            <select class="form-control" name="jk">
              <option>== Silahkan Pilih ==</option>
              <option value="Laki - Laki">Laki - Laki</option>
              <option value="Perempuan">Perempuan</option>
            </select>
          </div>
          <div class="form-group">
            <label>Tempat Lahir</label>
            <input type="text" class="form-control" name="tempat_lahir" placeholder="Masukkan Tempat Lahir ...">
          </div>
          <div class="form-group">
            <label>Tanggal Lahir</label>
            <input type="date" class="form-control" name="tgl_lahir" placeholder="Masukkan Tanggal Lahir ...">
          </div>
          <div class="form-group">
            <label>No Telepon/HP</label>
            <input type="text" class="form-control" name="no_telp" placeholder="Masukkan Nomor Telepon/HP ...">
          </div>
          <div class="form-group">
            <label>Asal Sekolah</label>
            <input type="text" class="form-control" name="asal_sekolah" placeholder="Masukkan Asal Sekolah ...">
          </div>
          <div class="form-group">
            <label>Kelas</label>
            <select class="form-control" name="id_kelas">
              <option value="show-all" selected="selected">= Kelas =</option>
              <?php
              require_once 'pengaturan.php';
              $stmt = $db->prepare('SELECT kelas.id_kelas, nama_kelas, abjad_kelas, guru.nip FROM mapel
                                                            JOIN kelas ON kelas.id_kelas=mapel.id_kelas
                                                            JOIN guru ON guru.nip=mapel.nip');
              $stmt->execute();
              ?>
              <?php while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) : ?>
                <?php extract($row); ?>
                <option value="<?php echo $row['id_kelas']; ?>">
                  <?php echo $row['nama_kelas']; ?> <?php echo $row['abjad_kelas']; ?>
                </option>
              <?php endwhile; ?>
            </select>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-success">Simpan</button>
          </div>


        </form>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
  <div class="col-md-6">
    <div class="box box-warning">
      <div class="box-header with-border">
        <h3 class="box-title"><b>Tips!</b></h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse">
            <p>Saya Mengerti/Saya kurang paham!</p></i>
          </button>
        </div>
        <!-- /.box-tools -->
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        Kata Sandi untuk login siswa sudah tergenerate otomatis.<br />
        Isikan Data Siswa sesuai pendaftarannya. NIS siswa akan otomatis mengikuti urutan kapan data
        siswa di input.
      </div>
      <!-- /.box-body -->
    </div>
  </div>
</div>