<?php
include "indo_date2.php";

include "config.php";
$query = mysqli_query($connection, "select * from siswa");
?>

<div class="box">
            <div class="box-header">
              <h3 class="Data Siswa">Data Siswa</h3> <a type="button" class="btn btn-success" href="./?page=tambah_siswa">Tambah Data Siswa</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>NIS</th>
                  <th>Nama Siswa</th>
                  <th>Alamat</th>
                  <th>Tempat/Tgl Lahir</th>
                  <th>No Telepon/HP</th>
                  <th>Asal Sekolah</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                <?php if (mysqli_num_rows($query) > 0) { ?>
                                    <?php
                                    $no = 1;
                                    while ($data = mysqli_fetch_array($query)) {
                                        ?>
                                    <tr>
                                        <!-- <td></td> -->
                                        <td><?php echo $no ?></td>
                                        <td><font face="trebuchet MS"><?php echo $data["nis"]; ?></font></td>
                                        <td><font face="trebuchet MS"><?php echo $data["nama_siswa"]; ?></font></td>
                                        <td><font face="trebuchet MS"><?php echo $data["alamat"]; ?></font></td>
                                        <td><font face="trebuchet MS"><?php echo $data["tempat_lahir"]; ?>, <?php echo indonesian_date_only($data["tgl_lahir"]); ?></font></td>
                                        <td><font face="trebuchet MS"><?php echo $data["no_telp"]; ?></font></td>
                                        <td><font face="trebuchet MS"><?php echo $data["asal_sekolah"]; ?></font></td>
                                        <td>
                                        <a href="pages/siswa/aksi_print.php?nis=<?php echo $data["nis"]; ?>&&jk=<?php echo $tata_usaha; ?>&&nama_ad=<?php echo $_SESSION["nam_ad"]; ?>"><i class="fa fa-print edu-checked-pro" aria-hidden="true" style="color: blue; font-size: 15px"></i></a> |
                                        <a href="?page=edit_siswa&nis=<?php echo $data['nis']; ?>" ><i class="fa fa-edit edu-checked-pro" aria-hidden="true" style="color: green; font-size: 15px"></i></a> |
                                        <a href="pages/siswa/hapus_siswa.php?nis=<?php echo $data['nis']; ?>" onClick="return confirm('Apakah anda yakin untuk menghapus data dari <?php echo $data['nama_siswa'] ?>? Setelah mengeklik OK, penghapusan tidak dapat di batalkan.');"  ><i class="fa fa-trash edu-checked-pro" aria-hidden="true" style="color: red; font-size: 15px"></i></a>
                                        </td>
                                    </tr>
                                            <?php 
                                            $no++;
                                        }
                                    }
                                    ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>

          <div class="modal modal-primary fade" id="modal-lihat">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                
                <h4 class="modal-title">Detail Siswa</h4>
              </div>
              <div class="modal-body">
                <p>
                  Nama Siswa : <?php echo $data["nama_siswa"]; ?><br/>
                  Tempat/Tanggal Lahir : <?php echo $data["tempat_lahir"]; ?>, <?php echo $data["tgl_lahir"]; ?><br/>
                  Jenis Kelamin : <?php echo $data["jk"]; ?><br/>
                  Asal Sekolah : <?php echo $data["asal_sekolah"]; ?><br/>
                  Nomor Telepon : <?php echo $data["no_telp"]; ?><br/>
                  <br/>
                  <br/>
                  <b>
                  NIS : <?php echo $data["nis"]; ?><br/>
                  Password : <?php echo $data["password"]; ?><br/>
                  </b>
                </p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                <a type="button" href="pages/siswa/aksi_print.php" class="btn btn-success">Print</a>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>