<footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        <b>Version</b> BETA
      </div>
      <strong>&copy; <script type="text/javascript">document.write(new Date().getFullYear());</script>. <?php echo $title; ?>. Allright Reserved.
    </div>
    <!-- /.container -->
  </footer>