<?php
if (empty($_GET['page'])) {
    include "dasbor.php";
} else {
    switch ($_GET['page']) {

            //pelanggan
        case ('data_guru'):
            include('pages/guru/data_guru.php');
            break;
        case ('tambah_guru'):
            include('pages/guru/tambah_guru.php');
            break;
        case ('edit_guru'):
            include('pages/guru/edit_guru.php');
            break;
        case ('data_siswa'):
            include('pages/siswa/data_siswa.php');
            break;
        case ('tambah_siswa'):
            include('pages/siswa/tambah_siswa.php');
            break;
        case ('edit_siswa'):
            include('pages/siswa/edit_siswa.php');
            break;
        case ('hapus_siswa'):
            include('pages/siswa/hapus_siswa.php');
            break;
        case ('data_mapel'):
            include('pages/mapel/data_mapel.php');
            break;
        case ('tambah_mapel'):
            include('pages/mapel/tambah_mapel.php');
            break;
        case ('edit_mapel'):
            include('pages/mapel/edit_mapel.php');
            break;
        case ('data_kelas'):
            include('pages/kelas/data_kelas.php');
            break;
        case ('tambah_kelas'):
            include('pages/kelas/tambah_kelas.php');
            break;
        case ('edit_kelas'):
            include('pages/kelas/edit_kelas.php');
            break;
        case ('my_profile'):
            include('pages/profil/profilku.php');
            break;
        case ('data_user'):
            include('pages/user/data_user.php');
            break;
        case ('tambah_user'):
            include('pages/user/tambah_user.php');
            break;
        case ('edit_user'):
            include('pages/user/edit_user.php');
            break;
        case ('proses_edit_user'):
            include('pages/user/proses_edit.php');
            break;
        case ('hapus_user'):
            include('pages/user/hapus_user.php');
            break;
        case ('laporan'):
            include('pages/laporan/laporan.php');
            break;
        case ('laporan_mapel'):
            include('pages/laporan/mapel.php');
            break;

        default:
            include('dasbor.php');
    }
}
