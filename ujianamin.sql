-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Waktu pembuatan: 17. Januari 2020 jam 16:57
-- Versi Server: 5.5.16
-- Versi PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ujianamin`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `guru`
--

CREATE TABLE IF NOT EXISTS `guru` (
  `nip` int(16) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `password` varchar(50) NOT NULL,
  `nama_guru` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `jk` varchar(12) NOT NULL,
  `no_telp` varchar(14) NOT NULL,
  `gelar` varchar(50) NOT NULL,
  PRIMARY KEY (`nip`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data untuk tabel `guru`
--

INSERT INTO `guru` (`nip`, `password`, `nama_guru`, `alamat`, `jk`, `no_telp`, `gelar`) VALUES
(0000000000000012, '6cZyO*WP', 'Fazal Said', 'Jogja', 'Pria', '082233439041', 'S.Kom');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelas`
--

CREATE TABLE IF NOT EXISTS `kelas` (
  `id_kelas` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nama_kelas` varchar(5) NOT NULL,
  `abjad_kelas` enum('A','B','C','D') NOT NULL,
  `jml_siswa` varchar(4) NOT NULL,
  PRIMARY KEY (`id_kelas`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data untuk tabel `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `nama_kelas`, `abjad_kelas`, `jml_siswa`) VALUES
(0001, 'X', 'A', '25'),
(0002, 'X', 'B', '25'),
(0003, 'X', 'C', '25'),
(0004, 'X', 'D', '25'),
(0005, 'XI', 'A', '20'),
(0006, 'XI', 'B', '20'),
(0007, 'XI', 'C', '20'),
(0008, 'XI', 'D', '20'),
(0009, 'XII', 'A', '16'),
(0010, 'XII', 'B', '16'),
(0011, 'XII', 'C', '16'),
(0012, 'XII', 'D', '16');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mapel`
--

CREATE TABLE IF NOT EXISTS `mapel` (
  `id_mapel` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nama_mapel` varchar(50) NOT NULL,
  `nip` int(16) unsigned zerofill NOT NULL,
  `id_kelas` int(4) unsigned zerofill NOT NULL,
  PRIMARY KEY (`id_mapel`),
  KEY `nip` (`nip`),
  KEY `id_kelas` (`id_kelas`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data untuk tabel `mapel`
--

INSERT INTO `mapel` (`id_mapel`, `nama_mapel`, `nip`, `id_kelas`) VALUES
(0010, 'TIK', 0000000000000012, 0004),
(0011, 'Bahasa Indonesia', 0000000000000012, 0003);

-- --------------------------------------------------------

--
-- Struktur dari tabel `siswa`
--

CREATE TABLE IF NOT EXISTS `siswa` (
  `nis` int(16) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `password` varchar(50) NOT NULL,
  `nama_siswa` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `jk` varchar(12) NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `no_telp` varchar(14) NOT NULL,
  `asal_sekolah` varchar(60) NOT NULL,
  `id_kelas` int(4) unsigned zerofill NOT NULL,
  PRIMARY KEY (`nis`),
  KEY `id_kelas` (`id_kelas`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data untuk tabel `siswa`
--

INSERT INTO `siswa` (`nis`, `password`, `nama_siswa`, `alamat`, `jk`, `tempat_lahir`, `tgl_lahir`, `no_telp`, `asal_sekolah`, `id_kelas`) VALUES
(0000000000000007, 'ayana123', 'Ayana', 'Jakarta', 'Perempuan', 'Tokyo', '2020-01-03', '123654789', 'smp1', 0012),
(0000000000000009, '1=q*KmDS', 'Itsuki', 'jogja', 'Perempuan', 'Yogyakarta', '2020-01-03', '082233439041', 'smp4', 0012);

-- --------------------------------------------------------

--
-- Struktur dari tabel `soal`
--

CREATE TABLE IF NOT EXISTS `soal` (
  `id_soal` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nama_soal` text NOT NULL,
  `a` text NOT NULL,
  `b` text NOT NULL,
  `c` text NOT NULL,
  `d` text NOT NULL,
  `id_mapel` int(4) unsigned zerofill NOT NULL,
  `id_kelas` int(4) unsigned zerofill NOT NULL,
  `nip` int(16) unsigned zerofill NOT NULL,
  `kunci_jawaban` varchar(3) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `aktif` varchar(1) NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id_soal`),
  KEY `id_mapel` (`id_mapel`),
  KEY `id_kelas` (`id_kelas`),
  KEY `nip` (`nip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tu_admin`
--

CREATE TABLE IF NOT EXISTS `tu_admin` (
  `id_ad` int(4) NOT NULL AUTO_INCREMENT,
  `nam_ad` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `jk` enum('Pria','Wanita') NOT NULL,
  PRIMARY KEY (`id_ad`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `tu_admin`
--

INSERT INTO `tu_admin` (`id_ad`, `nam_ad`, `username`, `password`, `jk`) VALUES
(1, 'Aminudin', 'tu_123456', 'amin123', 'Pria');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ujian`
--

CREATE TABLE IF NOT EXISTS `ujian` (
  `id_ujian` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_mapel` int(4) unsigned zerofill NOT NULL,
  `nis` int(16) unsigned zerofill NOT NULL,
  `nip` int(16) unsigned zerofill NOT NULL,
  `nilai` double NOT NULL,
  `tgl_ujian` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_ujian`),
  KEY `nis` (`nis`),
  KEY `nip` (`nip`),
  KEY `id_mapel` (`id_mapel`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `mapel`
--
ALTER TABLE `mapel`
  ADD CONSTRAINT `mapel_ibfk_4` FOREIGN KEY (`nip`) REFERENCES `guru` (`nip`),
  ADD CONSTRAINT `mapel_ibfk_5` FOREIGN KEY (`id_kelas`) REFERENCES `kelas` (`id_kelas`);

--
-- Ketidakleluasaan untuk tabel `siswa`
--
ALTER TABLE `siswa`
  ADD CONSTRAINT `siswa_ibfk_1` FOREIGN KEY (`id_kelas`) REFERENCES `guru` (`nip`);

--
-- Ketidakleluasaan untuk tabel `soal`
--
ALTER TABLE `soal`
  ADD CONSTRAINT `soal_ibfk_1` FOREIGN KEY (`id_mapel`) REFERENCES `mapel` (`id_mapel`),
  ADD CONSTRAINT `soal_ibfk_2` FOREIGN KEY (`id_kelas`) REFERENCES `kelas` (`id_kelas`),
  ADD CONSTRAINT `soal_ibfk_3` FOREIGN KEY (`nip`) REFERENCES `guru` (`nip`);

--
-- Ketidakleluasaan untuk tabel `ujian`
--
ALTER TABLE `ujian`
  ADD CONSTRAINT `ujian_ibfk_1` FOREIGN KEY (`nis`) REFERENCES `siswa` (`nis`),
  ADD CONSTRAINT `ujian_ibfk_2` FOREIGN KEY (`nip`) REFERENCES `guru` (`nip`),
  ADD CONSTRAINT `ujian_ibfk_3` FOREIGN KEY (`id_mapel`) REFERENCES `mapel` (`id_mapel`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
