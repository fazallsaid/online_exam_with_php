<?php
include "koneksi.php";
$nip = $_POST['nip'];

function randomPassword()
{
    // function untuk membuat password random 6 digit karakter

    $digit = 6;
    $karakter = "ABCDEFGHJKLMNPQRSTUVWXYZ23456789";

    srand((float) microtime() * 1000000);
    $i = 0;
    $pass = "";
    while ($i <= $digit - 1) {
        $num = rand() % 32;
        $tmp = substr($karakter, $num, 1);
        $pass = $pass . $tmp;
        $i++;
    }
    return $pass;
}

// membuat password baru secara random -> memanggil function randomPassword
$newPassword = randomPassword();

// perlu dibuat sebarang pengacak
$pengacak  = "NDJS3289JSKS190JISJI";

// mengenkripsi password dengan md5() dan pengacak
$newPasswordEnkrip = md5($pengacak . md5($newPassword) . $pengacak);

// mencari alamat email si user
$query = "SELECT * FROM guru WHERE nip = '$nip'";
$hasil = mysqli_query($kon, $query);
$data  = mysqli_fetch_array($hasil);
$alamatEmail = $data['email'];
$guru = $data['nama_guru'];

// title atau subject email
$title  = "Password Baru untuk Anda!";

// isi pesan email disertai password
$pesan  = "Karena Anda meminta untuk mereset password, maka kami akan memberikan password baru untuk Anda. \n \n
            NIP Anda : " . $nip . ". \nNama Anda : " . $guru . ". \nPassword Anda yang baru adalah " . $newPassword . "";

// header email berisi alamat pengirim
$header = "From: ujianonline.com";

// mengirim email
$kirimEmail = mail($alamatEmail, $title, $pesan, $header);

// cek status pengiriman email
if ($kirimEmail) {

    // update password baru ke database (jika pengiriman email sukses)
    $query = "UPDATE guru SET password = '$newPasswordEnkrip' WHERE nip = '$nip'";
    $hasil = mysqli_query($kon, $query);

    if ($hasil) echo "Password baru telah direset dan sudah dikirim ke email Anda.";
} else echo "Pengiriman password baru ke email gagal. Kami segera memperbaikinya.";
