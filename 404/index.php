<!DOCTYPE html>
<head>
<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, maximum-scale=1">
<title>Halaman Tidak Ditemukan</title>
<link href="style.css" rel="stylesheet" type="text/css">
</head>
<body>
<p>HTTP: <span>404</span></p>
<code><span>halaman_ini</span>.<em>tidak_ditemukan</em> = benar;</code>
<code>echo "Maaf, halaman ini tidak dapat ditemukan. Jika ini adalah karena Anda menggunakan smartphone, beralihlah menggunakan laptop atau personal computer.";</code>
<code>echo "Silahkan kembali ke halaman sebelumnya.";</code> 
<center><a href="../">Beranda</a></center>
<script src="animated.js"></script>
</body>
</html>