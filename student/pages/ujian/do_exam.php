<?php
include "../signin-student/koneksi.php";

$mapel = $_GET['mapel'];
$niip = $_GET['nip'];

$teacher = mysqli_query($kon, "SELECT mapel.*, guru.* FROM mapel JOIN guru ON mapel.nip=guru.nip WHERE mapel.nip='$niip' AND mapel.id_mapel='$mapel'");
$data_teacher = mysqli_fetch_array($teacher);
?>
<div class="box box-default">
	<div class="box-header with-border">
		<div class="row">
			<div class="col-md-3">
				<h3 class="box-title">
				Soal Ujian: <b><?php echo $data_teacher['nama_mapel']; ?></b><br/>
				Guru: <b><?php echo $data_teacher['nama_guru']; ?>, <?php echo $data_teacher['gelar']; ?></b>
				</h3>
			</div>
			<div class="col-md-3">

			</div>
			<div class="col-md-3">

			</div>
			<div class="col-md-3">
				Waktu kamu: <font size="4">
					<div id="servertime">
					</div>
				</font>
			</div>
		</div>
	</div>
	<div class="box-body">
		<?php

		$siswa = mysqli_query($kon, "SELECT * FROM siswa WHERE nis='$_SESSION[nis]'");
		$data_siswa = mysqli_fetch_array($siswa);

		// $halaman = 1;
		// $page = isset($_GET["halaman"]) ? (int) $_GET["halaman"] : 1;
		// $mulai = ($page > 1) ? ($page * $halaman) - $halaman : 0;
		// $result = mysqli_query($kon, "SELECT soal.*, mapel.*, kelas.*, guru.*
		// 							FROM soal
		// 							JOIN mapel ON soal.id_mapel=mapel.id_mapel
		// 							JOIN kelas ON soal.id_kelas=kelas.id_kelas
		// 							JOIN guru ON soal.nip=guru.nip
		// 							WHERE aktif='Y'
		// 							AND soal.id_mapel= '$mapel'
		// 							AND soal.id_kelas = '$data_siswa[id_kelas]'
		// 							ORDER BY RAND ()");
		// $total = mysqli_num_rows($result);
		// $pages = ceil($total / $halaman);
		// $query = mysqli_query($kon, "SELECT soal.*, mapel.*, kelas.*, guru.*
		// 							FROM soal
		// 							JOIN mapel ON soal.id_mapel=mapel.id_mapel
		// 							JOIN kelas ON soal.id_kelas=kelas.id_kelas
		// 							JOIN guru ON soal.nip=guru.nip
		// 							WHERE aktif='Y'
		// 							AND soal.id_mapel= '$mapel'
		// 							AND soal.id_kelas = '$data_siswa[id_kelas]'
		// 							ORDER BY RAND () 
		// 							LIMIT $mulai, $halaman") or die(mysql_error);
		// $no = $mulai + 1;

		?>

		<div style='width:100%; border: 1px solid #EBEBEB; overflow:scroll;height:700px;'>
			<table width="100%" border="0">
				<form name="frmSoal" method="POST" action="?page=jawab_soal" id="frmSoal">
					<?php
					$hasil = mysqli_query($kon, "SELECT soal.*, mapel.*, kelas.*, guru.*
									FROM soal
									JOIN mapel ON soal.id_mapel=mapel.id_mapel
									JOIN kelas ON soal.id_kelas=kelas.id_kelas
									JOIN guru ON soal.nip=guru.nip
									WHERE soal.aktif='Y'
									AND soal.id_mapel= '$mapel'
									AND soal.id_kelas = '$data_siswa[id_kelas]'
									AND soal.nip = '$niip'
									ORDER BY RAND ()");
					$jumlah = mysqli_num_rows($hasil);
					$urut = 0;

					$i = 1;
					while ($result = mysqli_fetch_array($hasil)) {
						$id = $result["id_soal"];
						$pertanyaan = $result["nama_soal"];
						$pilihan_a = $result["a"];
						$pilihan_b = $result["b"];
						$pilihan_c = $result["c"];
						$pilihan_d = $result["d"];

						?>

						<input type="hidden" name="id[]" id="id_soal" value=<?php echo $result["id_soal"]; ?>>
						<input type="hidden" name="jumlah" id="jumlah" value=<?php echo $jumlah; ?>>
						<input type="hidden" name="id_mapel" id="id_mapel" value=<?php echo $mapel; ?>>
						<input type="hidden" name="nip" id="nip" value=<?php echo $niip; ?>>
						<tr>
							<?php if ($i == 1) { ?>
								<div id='question<?php echo $i; ?>' class='cont'>
									<p class='questions' id="qname<?php echo $i; ?>"> <?php echo $i ?>.&#41;&nbsp;<?php echo $pertanyaan; ?></p>
									<?php
											if (!empty($result["gambar"])) {
												echo "<img src='../teacher/foto_soal/$result[gambar]' width='200' hight='200'>";
											}
											?>
									<br />
									A. <input type="radio" value="A" id='radio1_<?php echo $id; ?>' onclick="send(this)" name='pilihan[<?php echo $id; ?>]' /><?php echo $pilihan_a; ?>
									<br />
									B. <input type="radio" value="B" id='radio1_<?php echo $id; ?>' onclick="send(this)" name='pilihan[<?php echo $id; ?>]' /><?php echo $pilihan_b; ?>
									<br />
									C. <input type="radio" value="C" id='radio1_<?php echo $id; ?>' onclick="send(this)" name='pilihan[<?php echo $id; ?>]' /><?php echo $pilihan_c; ?>
									<br />
									D. <input type="radio" value="D" id='radio1_<?php echo $id; ?>' onclick="send(this)" name='pilihan[<?php echo $id; ?>]' /><?php echo $pilihan_d; ?>
									<br />
									<?php if ($i == $jumlah) { ?>
										<input type="submit" class="btn btn-success" name="submit" value="Simpan" onclick="return confirm('Apakah Anda yakin dengan jawaban Anda? Jika belum, klik batal.')">
									<?php } else { ?>
										<button id='next<?php echo $i; ?>' class='next btn btn-success' type='button'>Next</button>
									<?php } ?>
								</div>

							<?php } elseif ($i < 1 || $i < $jumlah) { ?>
								<div id='question<?php echo $i; ?>' class='cont'>
									<p class='questions' id="qname<?php echo $i; ?>"><?php echo $i ?>.&#41;&nbsp;<?php echo $pertanyaan; ?></p>
									<?php
											if (!empty($result["gambar"])) {
												echo "<img src='../teacher/foto_soal/$result[gambar]' width='200' hight='200'>";
											}
											?>
									<br />
									A. <input type="radio" value="A" id='radio1_<?php echo $id; ?>' onclick="send(this)" name='pilihan[<?php echo $id; ?>]' /><?php echo $pilihan_a; ?>
									<br />
									B. <input type="radio" value="B" id='radio1_<?php echo $id; ?>' onclick="send(this)" name='pilihan[<?php echo $id; ?>]' /><?php echo $pilihan_b; ?>
									<br />
									C. <input type="radio" value="C" id='radio1_<?php echo $id; ?>' onclick="send(this)" name='pilihan[<?php echo $id; ?>]' /><?php echo $pilihan_c; ?>
									<br />
									D. <input type="radio" value="D" id='radio1_<?php echo $id; ?>' onclick="send(this)" name='pilihan[<?php echo $id; ?>]' /><?php echo $pilihan_d; ?>
									<br />
									<button id='pre<?php echo $i; ?>' class='previous btn btn-success' type='button'>Previous</button>
									<button id='next<?php echo $i; ?>' class='next btn btn-success' type='button'>Next</button>
								</div>

							<?php } elseif ($i == $jumlah) { ?>
								<div id='question<?php echo $i; ?>' class='cont'>
									<p class='questions' id="qname<?php echo $i; ?>"><?php echo $i ?>.&#41;&nbsp;<?php echo $pertanyaan; ?></p>
									<?php
											if (!empty($result["gambar"])) {
												echo "<img src='../teacher/foto_soal/$result[gambar]' width='200' hight='200'>";
											}
											?>
									<br />
									A. <input type="radio" value="A" id='radio1_<?php echo $id; ?>' onclick="send(this)" name='pilihan[<?php echo $id; ?>]' /><?php echo $pilihan_a; ?>
									<br />
									B. <input type="radio" value="B" id='radio1_<?php echo $id; ?>' onclick="send(this)" name='pilihan[<?php echo $id; ?>]' /><?php echo $pilihan_b; ?>
									<br />
									C. <input type="radio" value="C" id='radio1_<?php echo $id; ?>' onclick="send(this)" name='pilihan[<?php echo $id; ?>]' /><?php echo $pilihan_c; ?>
									<br />
									D. <input type="radio" value="D" id='radio1_<?php echo $id; ?>' onclick="send(this)" name='pilihan[<?php echo $id; ?>]' /><?php echo $pilihan_d; ?>
									<br />
									<button id='pre<?php echo $i; ?>' class='previous btn btn-success' type='button'>Previous</button>
									<input type="submit" class="btn btn-success" name="simpan" id="simpan" value="Simpan" onclick="return confirm('Apakah Anda yakin dengan jawaban Anda? Jika belum, klik batal.')">
									<input type="submit" value="Submit" name="simpanoto" id="simpanoto" style="display: none;">
								</div>
						<?php }
							$i++;
						} ?>
						</tr>

				</form>


			</table>

		</div>
		<!-- /.box-body -->
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

<script>
	$('.cont').addClass('hide');
	count = $('.questions').length;
	$('#question' + 1).removeClass('hide');

	$(document).on('click', '.next', function() {
		element = $(this).attr('id');
		last = parseInt(element.substr(element.length - 1));
		nex = last + 1;
		$('#question' + last).addClass('hide');

		$('#question' + nex).removeClass('hide');
	});

	$(document).on('click', '.previous', function() {
		element = $(this).attr('id');
		last = parseInt(element.substr(element.length - 1));
		pre = last - 1;
		$('#question' + last).addClass('hide');

		$('#question' + pre).removeClass('hide');
	});
</script>

<script type="text/javascript">
	var detik = "5";
	var audio = new Audio('../function/notice_1_minute.wav');
	if (document.images) {
		parselimit = detik
	}

	function begintimer() {
		if (!document.images)
			return
		if (parselimit == 0) {
			clearInterval(begintimer);
			var data = $("#frmSoal").serialize();
			console.log(data);
			// var test = document.getElementById('simpanoto');
			// test.form.submit();
			// document.frmSoal.submit();
			// $("#simpan").click();
			// document.forms.submit();
			
			alert('Waktu Anda Habis !!!');
			$("#simpanoto").click();
			// document.getElementById("frmSoal").submit();
			// document.frmSoal.submit();
			// $.ajax({
			// 	type: 'POST',
			// 	url: '?page=jawab_soal',
			// 	data:data,
			// 	success: function(data){
			// 		// console.log(data);
			// 		//  location.href='?page=jawab_soal';
					
			// 	}
			// });
		} else {
			parselimit -= 1
			curhour = Math.floor(parselimit / 3600)
			curmin = Math.floor(parselimit / 60)
			cursec = parselimit % 60
			if (curhour != 0)
				curtime = curhour + " Jam " + curmin + " Menit " + cursec + " Detik "
			if (curmin != 0)
				curtime = curmin + " Menit " + cursec + " Detik "
			else
				curtime = "<font color='red'>" + cursec + " detik</font>"
			document.getElementById("servertime").innerHTML = curtime
			setTimeout("begintimer()", 1000)
		}
	}
</script>