<div class="box">
            <div class="box-header">
              <h3 class="Data User">Data Ujian Anda</h3>
            </div>
            <div class="box-body">
              <div class="row">
              
              <?php
        require '../function/kon.php';

        $siswa = mysqli_query($kon, "SELECT * FROM siswa WHERE nis='$_SESSION[nis]'");
        $data_siswa = mysqli_fetch_array($siswa);

        $query = mysqli_query($kon, "SELECT guru.*, soal.*, mapel.*, kelas.*, siswa.*
                                    FROM soal 
                                    JOIN guru ON guru.nip=soal.nip
                                    JOIN mapel ON soal.id_mapel=mapel.id_mapel
                                    JOIN kelas ON soal.id_kelas=kelas.id_kelas
                                    JOIN siswa ON siswa.id_kelas=kelas.id_kelas
                                    WHERE mapel.id_mapel NOT IN (SELECT id_mapel FROM ujian WHERE nis='$data_siswa[nis]')
                                    AND soal.id_kelas = '$data_siswa[id_kelas]'
                                    GROUP BY nama_mapel") or die("Gagal query");

        while ($ujian1 = mysqli_fetch_assoc($query)) {
            ?>
  <div class="col-md-5">
              <div class="small-box bg-blue">
            <div class="inner">
              <h3><?php echo $ujian1['nama_mapel']; ?></h3>

              <p>Nama Guru : <?php echo $ujian1['nama_guru']; ?>, <?php echo $ujian1['gelar']; ?></p>
            </div>
            <div class="icon">
              <i class="ion ion-book"></i>
            </div>
            <a href="?page=ujian&mapel=<?php echo $ujian1['id_mapel']; ?>&nip=<?php echo $ujian1['nip']; ?>" class="btn btn-success btn-block">
            Ikuti Ujian </a>
          </div>
        </div>
            <?php } ?>
              </div>
            </div>
</div>
            <!-- /.box-header -->
            
            <!-- /.box-body -->
</div>