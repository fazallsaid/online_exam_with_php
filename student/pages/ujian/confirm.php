<?php

$id_mapel = $_GET['id_mapel'];
$nip = $_GET['nip'];
//$id_soal = $_GET['id_soal'];
//$pilihan = $_GET['pilihan'];
$jumlah = $_GET['jumlah'];

	$score = 0;
	$benar = 0;
	$salah = 0;
	$kosong = 0;


	for ($i = 0; $i < $jumlah; $i++) {
		//id nomor soal
		$nomor = $id_soal[$i];

		//jika user tidak memilih jawaban
		if (empty($pilihan[$nomor])) {
			$kosong++;
		} else {
			//jawaban dari user
			$jawaban = $pilihan[$nomor];

			//cocokan jawaban user dengan jawaban di database
			$sql = "SELECT * FROM soal WHERE id_soal='$nomor' AND kunci_jawaban='$jawaban'";
			$query = mysqli_query($kon, $sql);

			$cek = mysqli_num_rows($query);

			if ($cek) {
				//jika jawaban cocok (benar)
				$benar++;
			} else {
				//jika salah
				$salah++;
			}
		}
		/*RUMUS
				Jika anda ingin mendapatkan Nilai 100, berapapun jumlah soal yang ditampilkan 
				hasil= 100 / jumlah soal * jawaban yang benar
				*/

		$result = mysqli_query($kon, "SELECT * FROM soal WHERE aktif='Y' AND id_mapel='$id_mapel' AND nip='$nip'");
		$jumlah_soal = mysqli_num_rows($result);
		$score = 100 / $jumlah_soal * $benar;
		$hasil = number_format($score, 1);
	}

?>

<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">Konfirmasi Mengerjakan Ujian</h3>
	</div>
	<div class="box-body">
		<h1>
			Waktu Anda habis.
		</h1>
		<br />
		<form action="?page=masukin_db" method="post">
			<input type="text" name="id_mapel" value="<?php echo $id_mapel; ?>">
			<input type="hidden" name="nis" value="<?php echo $_SESSION['nis']; ?>">
			<input type="hidden" name="nip" value="<?php echo $nip; ?>">
			<input type="hidden" name="nilai" value="<?php echo $hasil; ?>">
			<input type="submit" class="btn btn-success" value="Selesai" name="submit">
		</form>

	</div>
	<!-- /.box-body -->
</div>
</div>