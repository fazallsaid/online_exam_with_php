<?php

include 'hitung.php';
?>

<div class="box-header with-border">
	<h3 class="box-title">Soal Ujian</h3>
</div>
<div class="box-body">
	Jumlah Soal : <b><?php echo $jumlah; ?> Soal</b><br />
	Jawaban Benar : <b><?php echo $benar; ?> Soal</b><br />
	Jawaban Salah : <b><?php echo $salah; ?> Soal</b><br />
	Tidak Dijawab : <b><?php echo $kosong; ?> Soal</b><br />
	Nilai: <h1><b><?php echo $hasil; ?></b></h1><br />

	<form action="?page=masukin_db" method="post">
		<input type="hidden" name="id_mapel" value="<?php echo $id_mapel; ?>">
		<input type="hidden" name="nis" value="<?php echo $_SESSION['nis']; ?>">
		<input type="hidden" name="nip" value="<?php echo $nip; ?>">
		<input type="hidden" name="nilai" value="<?php echo $hasil; ?>">
		<input class="btn btn-primary" value="Selesai" type="submit">
	</form>
</div>
<!-- /.box-body -->
</div>
</div>