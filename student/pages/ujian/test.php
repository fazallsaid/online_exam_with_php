<div class="box box-default">
    <div class="box-header with-border">
        <div class="row">
            <div class="col-md-3">
                <h3 class="box-title">Soal Ujian</h3>
            </div>
            <div class="col-md-3">

            </div>
            <div class="col-md-3">

            </div>
            <div class="col-md-3">
                Waktu kamu: <font size="6">
                    <div id="servertime"></div>
                </font>
            </div>
        </div>
    </div>
    <div class="box-body">
        <?php
        include "../signin-student/koneksi.php";

        $mapel = $_GET['mapel'];

        $siswa = mysqli_query($kon, "SELECT * FROM siswa WHERE nis='$_SESSION[nis]'");
        $data_siswa = mysqli_fetch_array($siswa);

        $halaman = 1;
        $page = isset($_GET["halaman"]) ? (int) $_GET["halaman"] : 1;
        $mulai = ($page > 1) ? ($page * $halaman) - $halaman : 0;
        $result = mysqli_query($kon, "SELECT soal.*, mapel.*, kelas.*, guru.*
									FROM soal
									JOIN mapel ON soal.id_mapel=mapel.id_mapel
									JOIN kelas ON soal.id_kelas=kelas.id_kelas
									JOIN guru ON soal.nip=guru.nip
									WHERE aktif='Y'
									AND soal.id_mapel= '$mapel'
									AND soal.id_kelas = '$data_siswa[id_kelas]'
									ORDER BY RAND ()");
        $total = mysqli_num_rows($result);
        $pages = ceil($total / $halaman);
        $query = mysqli_query($kon, "SELECT soal.*, mapel.*, kelas.*, guru.*
									FROM soal
									JOIN mapel ON soal.id_mapel=mapel.id_mapel
									JOIN kelas ON soal.id_kelas=kelas.id_kelas
									JOIN guru ON soal.nip=guru.nip
									WHERE aktif='Y'
									AND soal.id_mapel= '$mapel'
									AND soal.id_kelas = '$data_siswa[id_kelas]'
									ORDER BY RAND () 
									LIMIT $mulai, $halaman") or die(mysql_error);
        $no = $mulai + 1;

        ?>
        <div style='width:100%; border: 1px solid #EBEBEB; overflow:scroll;height:700px;'>
            <table width="100%" border="0">
                <form name="form1" method="post" action="?page=jawab_soal">
                    <?php
                    $hasil = mysqli_query($kon, "SELECT soal.*, mapel.*, kelas.*, guru.*
									FROM soal
									JOIN mapel ON soal.id_mapel=mapel.id_mapel
									JOIN kelas ON soal.id_kelas=kelas.id_kelas
									JOIN guru ON soal.nip=guru.nip
									WHERE aktif='Y'
									AND soal.id_mapel= '$mapel'
									AND soal.id_kelas = '$data_siswa[id_kelas]'
									ORDER BY RAND ()");
                    $jumlah = mysqli_num_rows($hasil);
                    $urut = 0;
                    while ($row = mysqli_fetch_array($query)) {
                        $id = $row["id_soal"];
                        $pertanyaan = $row["nama_soal"];
                        $pilihan_a = $row["a"];
                        $pilihan_b = $row["b"];
                        $pilihan_c = $row["c"];
                        $pilihan_d = $row["d"];

                        ?>

                        <input type="hidden" name="id[]" value=<?php echo $id; ?>>
                        <input type="hidden" name="jumlah" value=<?php echo $jumlah; ?>>
                        <input type="hidden" name="id_mapel" value=<?php echo $mapel; ?>>
                        <tr>
                            <td width="17">
                                <font color="#000000"><?php echo $no ?>. </font>
                            </td>
                            <td width="430">
                                <font color="#000000"><?php echo "$pertanyaan"; ?></font>
                            </td>
                        </tr>
                        <?php
                        if (!empty($row["gambar"])) {
                            echo "<tr><td></td><td><img src='../teacher/foto_soal/$row[gambar]' width='200' hight='200'></td></tr>";
                        }
                        ?>
                        <tr>
                            <td height="21">
                                <font color="#000000">&nbsp;</font>
                            </td>
                            <td>
                                <font color="#000000">
                                    A. <input name="pilihan[<?php echo $id; ?>]" type="radio" value="A">
                                    <?php echo "$pilihan_a"; ?></font>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <font color="#000000">&nbsp;</font>
                            </td>
                            <td>
                                <font color="#000000">
                                    B. <input name="pilihan[<?php echo $id; ?>]" type="radio" value="B">
                                    <?php echo "$pilihan_b"; ?></font>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <font color="#000000">&nbsp;</font>
                            </td>
                            <td>
                                <font color="#000000">
                                    C. <input name="pilihan[<?php echo $id; ?>]" type="radio" value="C">
                                    <?php echo "$pilihan_c"; ?></font>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <font color="#000000">&nbsp;</font>
                            </td>
                            <td>
                                <font color="#000000">
                                    D. <input name="pilihan[<?php echo $id; ?>]" type="radio" value="D">
                                    <?php echo "$pilihan_d"; ?></font>
                            </td>
                        </tr>

                    <?php
                    }
                    ?>
                    <tr>
                        <td>&nbsp;</td>

                        <td><br />
                            <?php
                            if ($page < 2) {
                                ?>
                                <a href="?page=kerjakan_soal&mapel=<?php echo $mapel ?>&halaman=<?php echo $page + 1; ?>"><input type="button" class="btn btn-primary" value="Next" /></a>
                            <?php
                            } elseif ($page = $total) {
                                ?>
                                <a href="?page=kerjakan_soal&mapel=<?php echo $mapel ?>&halaman=<?php echo $page - 1; ?>"><input type="button" class="btn btn-warning" value="Pervious" /></a>
                                <input type="submit" class="btn btn-success" name="submit" value="Simpan" onclick="return confirm('Apakah Anda yakin dengan jawaban Anda? Jika belum, klik batal.')"></td>
                        <?php
                        } else {
                            ?>
                            <a href="?page=kerjakan_soal&mapel=<?php echo $mapel ?>&halaman=<?php echo $page - 1; ?>"><input type="button" class="btn btn-warning" value="Pervious" /></a>
                            <a href="?page=kerjakan_soal&mapel=<?php echo $mapel ?>&halaman=<?php echo $page + 1; ?>"><input type="button" class="btn btn-primary" value="Next" /></a>

                        <?php } ?>
                    </tr>

                </form>
            </table>

        </div>
        <!-- /.box-body -->
    </div>
</div>