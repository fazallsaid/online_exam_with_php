<?php

include "../signin-student/koneksi.php";

// if (isset($_POST['simpan'])) {
	$pilihan = isset($_POST['pilihan']) ? $_POST['pilihan'] : null;
	$id_soal = isset($_POST['id']) ? $_POST['id'] : null;
	$jumlah = isset($_POST['jumlah']) ? $_POST['jumlah'] : null;
	$id_mapel = isset($_POST['id_mapel']) ? $_POST['id_mapel'] : null;
	$nip = isset($_POST['nip']) ? $_POST['nip'] : null;

	// $pilihan = isset($_POST["pilihan"]) ? $_POST;
	// $id_soal = $_POST["id"];
	// $jumlah = $_POST['jumlah'];
	// $id_mapel = $_POST['id_mapel'];
	// $nip = $_POST['nip'];

	$score = 0;
	$benar = 0;
	$salah = 0;
	$kosong = 0;


	for ($i = 0; $i < $jumlah; $i++) {
		//id nomor soal
		$nomor = $id_soal[$i];

		//jika user tidak memilih jawaban
		if (empty($pilihan[$nomor])) {
			$kosong++;
		} else {
			//jawaban dari user
			$jawaban = $pilihan[$nomor];

			//cocokan jawaban user dengan jawaban di database
			$sql = "SELECT * FROM soal WHERE id_soal='$nomor' AND kunci_jawaban='$jawaban'";
			$query = mysqli_query($kon, $sql);

			$cek = mysqli_num_rows($query);

			if ($cek) {
				//jika jawaban cocok (benar)
				$benar++;
			} else {
				//jika salah
				$salah++;
			}
		}
		/*RUMUS
				Jika anda ingin mendapatkan Nilai 100, berapapun jumlah soal yang ditampilkan 
				hasil= 100 / jumlah soal * jawaban yang benar
				*/

		$result = mysqli_query($kon, "SELECT * FROM soal WHERE aktif='Y' AND id_mapel='$id_mapel' AND nip='$nip'");
		$jumlah_soal = mysqli_num_rows($result);
		$score = 100 / $jumlah_soal * $benar;
		$hasil = number_format($score, 1);
		// echo json_encode(array('jumlah_soal'=> 100));
	}
	?>

	<div class="box box-default">

		<div class="box-header with-border">
			<center>
				<h3 class="box-title" style="color:green"><b>Anda Telah Selesai Mengerjakan Soal</b></h3>
			</center>
		</div>
	<?php
	
//Lakukan Penyimpanan Kedalam Database
