<?php
require '../function/kon.php';
$mapel = $_GET['mapel'];
$niip = $_GET['nip'];
$teacher = mysqli_query($kon, "SELECT * FROM mapel JOIN guru ON mapel.nip=guru.nip WHERE mapel.nip='$niip' AND mapel.id_mapel='$mapel'");
$data_teacher = mysqli_fetch_array($teacher);
?>

<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">Soal Ujian <?php echo $data_teacher['nama_mapel']; ?></h3>
	</div>
	<div class="box-body">
		<h1>Peraturan Ujian</h1>
		<br /><br />
		<b>Berikut peraturan yang harus kamu patuhi dalam mengerjakan ujian ini:</b>
		<br/>
		<br/>
		1. Dilarang menyontek, walaupun ujian ini menggunakan komputer.
		<br/>
		2. Beritahu pengawas jika kamu menemukan soal yang kurang jelas, atau ada kesalahan penulisan.
		<br/>
		3. Pastikan kamu mengerjakan soal sebelum waktunya habis.
		<br/>
		4. Jika kamu tidak mengerjakan, atau tidak selesai sampai waktu habis, maka akan tersubmit otomatis dengan hasil apa adanya.
		<br/>
		5. Kerjakan soal dengan sungguh - sungguh, dan jangan lupa berdoa!
		<br />
		<br/>
		<a class="btn btn-success" href="?page=kerjakan_soal&mapel=<?php echo $mapel ?>&nip=<?php echo $niip; ?>">Saya mengerti</a><br/><br/>
	</div>
</div>