<div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">

              <h3 class="profile-username text-center"><b><?php echo $_SESSION['nama_siswa']; ?></b></h3>

              <p class="text-muted text-center"><?php echo $_SESSION['nis']; ?> - Siswa</p>

              <p class="text-muted text-center"><?php echo $_SESSION['alamat']; ?></p>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tentang Saya</h3>
            </div>
            <!-- box-header -->
            <div class="box-body">
              <strong><i class="fa fa-book margin-r-5"></i> Asal Sekolah</strong>

              <p class="text-muted">
              <?php echo $_SESSION['asal_sekolah']; ?>
              </p>

              <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> Tempat/Tanggal Lahir</strong>

              <p class="text-muted"><?php echo $_SESSION['tempat_lahir']; ?>, <?php echo indonesian_date_only($_SESSION['tgl_lahir']); ?></p>

            </div>
            <!-- box-body -->
          </div>
          <!-- box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <!-- <li class="active"><a href="#activity" data-toggle="tab">Activity</a></li>
              <li><a href="#timeline" data-toggle="tab">Timeline</a></li> -->
              <li ><a href="#settings" data-toggle="tab">Pengaturan Akun</a></li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="settings">
                <form class="form-horizontal" action="?page=update_profile" method="post">
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">NIS</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputName" name="nis" value="<?php echo $_SESSION['nis']; ?>" readonly><i>NIS tidak dapat diganti.</i>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Kata Sandi</label>
                    <div class="col-sm-10">
                      <input type="password" class="form-control" id="inputPassword" name="password" value="<?php echo $_SESSION['password']; ?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Nama Anda</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputName" name="nama_siswa" value="<?php echo $_SESSION['nama_siswa']; ?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputExperience" class="col-sm-2 control-label">Alamat</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" name="alamat" id="inputExperience"><?php echo $_SESSION['alamat']; ?></textarea>
                    </div>
                  </div>

                  <input type="hidden" class="form-control" id="inputName" name="jk" value="<?php echo $_SESSION['jk']; ?>">

                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Tempat Lahir</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputName" name="tempat_lahir" value="<?php echo $_SESSION['tempat_lahir']; ?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Tanggal Lahir</label>
                    <div class="col-sm-10">
                      <input type="date" class="form-control" id="inputName" name="tgl_lahir" value="<?php echo $_SESSION['tgl_lahir']; ?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Nomor Telepon/HP</label>
                    <div class="col-sm-10">
                      <input type="number" class="form-control" id="inputName" name="no_telp" value="<?php echo $_SESSION['no_telp']; ?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Asal Sekolah</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputName" name="asal_sekolah" value="<?php echo $_SESSION['asal_sekolah']; ?>">
                    </div>
                  </div>

                      <input type="hidden" class="form-control" id="inputName" name="id_kelas" value="<?php echo $_SESSION['id_kelas']; ?>" readonly>
                    

                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-success">Ubah Profil</button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>