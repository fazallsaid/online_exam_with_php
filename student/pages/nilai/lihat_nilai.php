<div class="box box-default">
          <div class="box-header with-border">
            <h3 class="box-title">Soal Ujian</h3>
          </div>
          <div class="box-body">
			Halo <?php echo $_SESSION['nama_siswa']; ?>, ini adalah daftar nilaimu!<br/>
			<br/>
			<div class="row">
			<?php
					require '../function/kon.php';
					$query = mysqli_query($kon, "select siswa.*, guru.*, mapel.*, ujian.*
					from ujian
					join siswa on ujian.nis=siswa.nis
					join guru on ujian.nip=guru.nip
					join mapel on ujian.id_mapel=mapel.id_mapel
					where ujian.nis='$_SESSION[nis]'") or die("Gagal query");
					while ($nilai = mysqli_fetch_assoc($query)) {
						if ($nilai > 0){
					?>
				<div class="col-md-3 col-sm-6 col-xs-12">
						<div class="info-box">
							<span class="info-box-icon bg-aqua"><i class="fa fa-envelope-o"></i></span>

							<div class="info-box-content">
								<span class="info-box-text"><?php echo $nilai['nama_mapel']; ?></span>
								<span class="info-box-number"><font size="7"><?php echo $nilai['nilai']; ?></font></span>
								<span class="info-box-text"><font size="2"><?php echo indonesian_date($nilai['tgl_ujian']); ?></font></span>
							</div>
						</div>
				</div>
				<?php
				}else{ ?>
					Kamu belum mengerjakan soal apapun.
				<?php } } ?>
			</div>
		  </div>
</div>