<?php
if (empty($_GET['page'])) {
    include "dasbor.php";
} else {
    switch ($_GET['page']) {
        
        //pelanggan
        case ('kerjakan_soal'):
            include('pages/ujian/do_exam.php');
            break;
        case ('jawab_soal'):
            include('pages/ujian/jawab.php');
            break;
        case ('masukin_db'):
            include('pages/ujian/masuk_db.php');
            break;
        case ('lihat_nilai'):
            include('pages/nilai/lihat_nilai.php');
            break;
        case ('konfirmasi'):
            include('pages/ujian/confirm.php');
            break;
        case ('ujian'):
            include('pages/ujian/poin_ujian.php');
            break;
        case ('my_profile'):
            include('pages/profil/profilku.php');
            break;
        case ('update_profile'):
            include('pages/profil/proses_edit.php');
            break;
        case ('list_ujian'):
            include('pages/ujian/data_ujian.php');
            break;

        default:
            include('dasbor.php');
    }
}
?>