<div class="alert alert-success alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <h4><i class="icon fa fa-check"></i> Sukses!</h4>
  <?php echo $notif; ?>
</div>

<div class="row">
  <div class="col-md-6">
    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">Informasi Pengguna</h3>
      </div>
      <div class="box-body">
        Nama: <b><?php echo $_SESSION['nama_siswa']; ?><br /></b>
        NIS: <b><?php echo $_SESSION['nis']; ?><br /></b><br /><br />
      </div>
      <!-- /.box-body -->
    </div>
  </div>
  <div class="col-md-6">
    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">Daftar Ujian</h3>
      </div>
      <div class="box-body">
        <?php
        require '../function/kon.php';

        $siswa = mysqli_query($kon, "SELECT * FROM siswa WHERE nis='$_SESSION[nis]'");
        $data_siswa = mysqli_fetch_array($siswa);

        $query1 = mysqli_query($kon, "SELECT guru.*, soal.*, mapel.*, kelas.*, siswa.*
                                    FROM soal 
                                    JOIN guru ON guru.nip=soal.nip
                                    JOIN mapel ON soal.id_mapel=mapel.id_mapel
                                    JOIN kelas ON soal.id_kelas=kelas.id_kelas
                                    JOIN siswa ON siswa.id_kelas=kelas.id_kelas
                                    WHERE mapel.id_mapel NOT IN (SELECT id_mapel FROM ujian WHERE nis='$data_siswa[nis]')
                                    AND soal.id_kelas = '$data_siswa[id_kelas]'
                                    GROUP BY nama_mapel") or die("Gagal query");

        while ($mapel = mysqli_fetch_assoc($query1)) {
            ?>
            <b><?php echo $mapel['nama_mapel']; ?></b><br />
            <i>Untuk mengikuti ujian, silahkan klik menu <b>Ujian</b>.<i>
            <hr />
          <?php
          } ?>

      </div>
      <!-- /.box-body -->
    </div>
  </div>
</div>